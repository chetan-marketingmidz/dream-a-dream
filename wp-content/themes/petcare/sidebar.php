<?php
/*

The Right Sidebar displayed on page templates.

*/
?>
<ul id="sidebar">
	<?php
    if (function_exists('dynamic_sidebar')) {
        dynamic_sidebar("blog-page");
    } ?>
</ul>