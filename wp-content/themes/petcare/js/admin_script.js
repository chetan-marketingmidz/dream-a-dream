// JavaScript Document
/********************* Duplicate post ki jquery ***************************/
jQuery(document).ready(function($) {
    "use strict";
    $( "#title" ).change(function() {
        if($(this).val() !== ""){
            var this_post_id;
            this_post_id = "";
            if($("#post_ID").val()){
                this_post_id = $("#post_ID").val();
            }
            var data = {
                'action': 'my_action',
                'this_convidado_title': $(this).val(),
                'post_type': 'your_post_type',
                'post_ID' : this_post_id
            };
            // We can also pass the url value separately from ajaxurl for front end AJAX implementations
            jQuery.post(ajax_object.ajax_url, data, function(response) {
                if(response > 0){
                    alert('There is a post with this same Title!');
                    $("#title").val("");
                    $("#post").submit();
                    // I do the form#post submission because I could not find the trigger to use the autosave - would be better with the auto save
                    // This is needed to avoid save post drafts with the unwanted title
                }
            });
        }
    });
});   