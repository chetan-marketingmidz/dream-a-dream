<?php
header("Content-type: text/css"); 

define( 'WP_USE_THEMES', false );
 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
 

global $petcare_option; ?>

if (jQuery("#map").length > 0){
	jQuery('#map').gmap3({
		map: {
		    options:{
		        zoom:15,
		        center: [<?php if ($petcare_option['Long']){ echo $petcare_option['Long']; } else { echo "51.733472"; } ?>, <?php if ($petcare_option['Lat']){ echo $petcare_option['Lat']; } else { echo "0.678792"; } ?>],
		        mapTypeId: google.maps.MapTypeId.MAP,
		        mapTypeControl: true,
		        mapTypeControlOptions: {
		          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		        },
		        navigationControl: true,
		        scrollwheel: true,
		        streetViewControl: true,
		        disableDefaultUI: false
		    }
		},
		marker:{
		    latLng: [<?php if ($petcare_option['Long']){ echo $petcare_option['Long']; } else { echo "51.733472"; } ?>, <?php if ($petcare_option['Lat']){ echo $petcare_option['Lat']; } else { echo "0.678792"; } ?>],
		    options: {
			    icon: new google.maps.MarkerImage(
			        "<?php if (isset($petcare_option['Pin']['url'])) { echo $petcare_option['Pin']['url']; } else { echo get_template_directory_uri('template_directory') . '/images/map-pin.png'; } ?>", new google.maps.Size(<?php if ($petcare_option['PinWidth']){ echo $petcare_option['PinWidth']; } else { echo "136"; } ?>, <?php if ($petcare_option['PinHeight']){ echo $petcare_option['PinHeight']; } else { echo "136"; } ?>, "px", "px")
			    )
		    }
		 }
		}

	);
}