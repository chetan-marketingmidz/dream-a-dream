<?php
/*

The template for displaying Comments.

*/
// Return early no password has been entered for protected posts.
if (post_password_required()) {
    return;
} ?>
<div id="comments" class="comments-area">
    <?php if (have_comments()) : ?>
    	<h3 id="responses"><?php comments_number( 'no comments', '1 comment', '% comments' ); ?></h3>
        <?php wp_list_comments( 'type=comment&callback=inkd_comments' ); ?>

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
            <nav id="comment-nav-below" class="navigation" role="navigation">
                <div class="nav-previous">
                    <?php previous_comments_link( _e('&larr; Older Comments', 'bootstrapwp')); ?>
                </div>
                <div class="nav-next">
                    <?php next_comments_link(_e('Newer Comments &rarr;', 'bootstrapwp')); ?>
                </div>
            </nav>
        <?php endif; // check for comment navigation ?>

        <?php elseif (!comments_open() && '0' != get_comments_number() && post_type_supports(get_post_type(), 'comments')) : ?>
            <p class="nocomments"><?php _e('Comments are closed.', 'bootstrapwp'); ?></p>
    <?php endif; ?>
    <div id="comments">
	    <?php
		    ob_start();
		    comment_form();
		    echo str_replace('value="Post Comment"','value="Post Comment" class="btn btn-default btn-green"',ob_get_clean());
		?>
	</div>
</div><!-- #comments .comments-area -->