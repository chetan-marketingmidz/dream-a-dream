<?php
/*

Default Footer

*/
?>
<?php global $petcare_option; ?>
<!-- Footer -->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer column 1')) : ?>
						<h6>Footer column 1</h6>
						<p>Please apply a text widget in you dashboard.</p>
					<?php endif; ?>
				</div>
				<div class="col-md-3 blog">
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer column 2')) : ?>
						<h6>Footer column 2</h6>
						<p>Please apply a blog posts widget in you dashboard.</p>
					<?php endif; ?>
				</div>
				<div class="col-md-3">
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer column 3')) : ?>
						<h6>Footer column 3</h6>
						<p>Please apply a text widget in you dashboard.</p>
					<?php endif; ?>
				</div>
				<div class="col-md-3 contact-info">
					<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer column 4')) : ?>
						<h6>Footer column 4</h6>
						<p>Please apply a text widget in you dashboard.</p>
					<?php endif; ?>
					<p class="social">
						<?php if ($petcare_option['Twitter-url'] != "") { ?>
							<a href="<?php echo $petcare_option['Twitter-url']; ?>" title="Follow us on Twitter" class="twitter"></a>
						<?php } ?>
						<?php if ($petcare_option['Facebook-url'] != "") { ?>
							<a href="<?php echo $petcare_option['Facebook-url']; ?>" title="Like us on Facebook" class="facebook"></a>
						<?php } ?>
						<?php if ($petcare_option['LinkedIn-url'] != "") { ?>
							<a href="<?php echo $petcare_option['LinkedIn-url']; ?>" title="Friend us on LinkedIn" class="linkedin"></a>
						<?php } ?>
						<?php if ($petcare_option['Pinterest-url'] != "") { ?>
							<a href="<?php echo $petcare_option['Pinterest-url']; ?>" title="Follow us on Pinterest" class="pinterest"></a>
						<?php } ?>
						<?php if ($petcare_option['GooglePlus-url'] != "") { ?>
							<a href="<?php echo $petcare_option['GooglePlus-url']; ?>" title="Follow us on Google +" class="googleplus"></a>
						<?php } ?>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 copyright">
					<p><?php echo $petcare_option['Footer-text']; ?></p>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php if (isset($petcare_option['Analytics-code'])) { echo $petcare_option['Analytics-code']; } ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php wp_footer(); ?>
</body>
</html>