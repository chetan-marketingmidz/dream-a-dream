<?php

/** SINGLE ADOPTION PAGE **/
function puppies_metaboxes($metaboxes) {
    // Declare your sections
    $boxSections = array();
    $boxSections[] = array(
        //'title'         => __('General Settings', 'redux-framework-demo'),
        //'icon'          => 'el-icon-home', // Only used with metabox position normal or advanced
        'fields'        => array(
        	array(
                'id'        => 'opt-slides',
                'type'      => 'slides',
                'title'     => __('Puppies gallery', 'redux-framework-demo'),
                'subtitle'  => __('Upload images of the animal up for puppies.', 'redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'placeholder'   => array(
                    'title'         => __('This is a title', 'redux-framework-demo'),
                    'description'   => __('Description Here', 'redux-framework-demo'),
                    'url'           => __('Give us a link!', 'redux-framework-demo'),
                ),
            ),
        ),          
    );

    // Declare your metaboxes
    if (!isset($metaboxes) && !is_array($metaboxes)) {$metaboxes = array(); }
    $metaboxes[] = array(
        'id'            => 'puppies',
        'title'         => __( 'Options', 'fusion-framework' ),
        'post_types'    => array( 'puppies' ),
        //'page_template' => array('page-test.php'), // Visibility of box based on page template selector
        //'post_format' => array('image'), // Visibility of box based on post format
        'position'      => 'advanced', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low - Priorities of placement
        'sections'      => $boxSections,
    );

    return $metaboxes;
}

add_action("redux/metaboxes/{$redux_opt_name}/boxes", "puppies_metaboxes");

?>
