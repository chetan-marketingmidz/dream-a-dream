<?php

/** SINGLE TEAM PAGE */
function team_metaboxes($metaboxes) {
    // Declare your sections
    $boxSections = array();
    $boxSection[] = array(
        //'title'         => __('General Settings', 'redux-framework-demo'),
        //'icon'          => 'el-icon-home', // Only used with metabox position normal or advanced
        'fields'        => array(
        	array(
	            'id'    => 'Facebook-link',
	            'type'  => 'text',
	            'title' => 'Facebook URL',
	        ),
	        array(
	            'id'    => 'Twitter-link',
	            'type'  => 'text',
	            'title' => 'Twitter URL',
	        ),
        ),
    );

    // Declare your metaboxes
    if (!isset($metaboxes) && !is_array($metaboxes)) {$metaboxes = array(); }
    $metaboxes[] = array(
        'id'            => 'team',
        'title'         => __( 'Social media', 'fusion-framework' ),
        'post_types'    => array( 'team' ),
        //'page_template' => array('PricePage.php'), // Visibility of box based on page template selector
        //'post_format' => array('image'), // Visibility of box based on post format
        'position'      => 'side', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low - Priorities of placement
        'sections'      => $boxSection,
    );

    return $metaboxes;
}

add_action("redux/metaboxes/{$redux_opt_name}/boxes", "team_metaboxes");

?>