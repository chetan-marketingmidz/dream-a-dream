<?php

/** SINGLE hero PAGE */
function hero_metaboxes($metaboxes) {
    // Declare your sections
    $boxSections = array();
    $boxSection[] = array(
        //'title'         => __('General Settings', 'redux-framework-demo'),
        //'icon'          => 'el-icon-home', // Only used with metabox position normal or advanced
        'fields'        => array(
        	array(
	            'id'    => 'button-text',
	            'type'  => 'text',
	            'title' => 'Button text',
	        ),
	        array(
                'id'        => 'opt-select-pages',
                'type'      => 'select',
                'data'      => 'pages',
                'title'     => __('Link', 'redux-framework-demo'),
                //'subtitle'  => __('No validation can be done on this field type', 'redux-framework-demo'),
                'desc'      => __('Link to your booking page.', 'redux-framework-demo'),
            ),
        ),
    );

    // Declare your metaboxes
    if (!isset($metaboxes) && !is_array($metaboxes)) {$metaboxes = array(); }
    $metaboxes[] = array(
        'id'            => 'Hero',
        'title'         => __( 'Options', 'fusion-framework' ),
        'post_types'    => array( 'hero' ),
        //'page_template' => array('PricePage.php'), // Visibility of box based on page template selector
        //'post_format' => array('image'), // Visibility of box based on post format
        'position'      => 'side', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low - Priorities of placement
        'sections'      => $boxSection,
    );

    return $metaboxes;
}

add_action("redux/metaboxes/{$redux_opt_name}/boxes", "hero_metaboxes");

?>