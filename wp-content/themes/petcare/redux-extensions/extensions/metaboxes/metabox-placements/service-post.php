<?php

/** SINGLE service PAGE **/
function service_metaboxes($metaboxes) {
    // Declare your sections
    $boxSections = array();
    $boxSections[] = array(
        //'title'         => __('General Settings', 'redux-framework-demo'),
        //'icon'          => 'el-icon-home', // Only used with metabox position normal or advanced
        'fields'        => array(
        	array(
                'id'        => 'opt-media',
                'type'      => 'media',
                'url'       => true,
                'title'     => __('Upload main image', 'redux-framework-demo'),
                'compiler'  => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'      => __('Upload service image to sit beside the content.', 'redux-framework-demo'),
                //'subtitle'  => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
                //'default'   => array('url' => 'http://s.wordpress.org/style/images/codeispoetry.png'),
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
            array(
                'id'        => 'ext-service-link',
                'type'      => 'text',
                'title'     => __('Link external to page', 'redux-framework-demo'),
                'subtitle'  => __('', 'redux-framework-demo'),
                'desc'      => __('If left empty, this will link to this service page.', 'redux-framework-demo'),
                'default'   => '',
            ),
        ),         
    );

    // Declare your metaboxes
    if (!isset($metaboxes) && !is_array($metaboxes)) {$metaboxes = array(); }
    $metaboxes[] = array(
        'id'            => 'service',
        'title'         => __( 'Options', 'fusion-framework' ),
        'post_types'    => array( 'service' ),
        //'page_template' => array('page-test.php'), // Visibility of box based on page template selector
        //'post_format' => array('image'), // Visibility of box based on post format
        'position'      => 'side', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low - Priorities of placement
        'sections'      => $boxSections,
    );

    return $metaboxes;
}

add_action("redux/metaboxes/{$redux_opt_name}/boxes", "service_metaboxes");

?>
