<?php

/** SINGLEPRICE PAGE **/
function price_metaboxes($metaboxes) {
    // Declare your sections
    $boxSections = array();
    $boxSections[] = array(
        //'title'         => __('General Settings', 'redux-framework-demo'),
        //'icon'          => 'el-icon-home', // Only used with metabox position normal or advanced
        'fields'        => array(
        	array(
	            'id'    => 'Post-Price',
	            'type'  => 'text',
	            'title' => 'Price',
	        ),
	        array(
	            'id'    => 'button-text',
	            'type'  => 'text',
	            'title' => 'Button text',
	        ),
	        array(
                'id'        => 'opt-select-pages',
                'type'      => 'select',
                'data'      => 'pages',
                'title'     => __('Link', 'redux-framework-demo'),
                //'subtitle'  => __('No validation can be done on this field type', 'redux-framework-demo'),
                'desc'      => __('Link to your booking page.', 'redux-framework-demo'),
            ),
        ),
    );

    // Declare your metaboxes
    if (!isset($metaboxes) && !is_array($metaboxes)) {$metaboxes = array(); }
    $metaboxes[] = array(
        'id'            => 'price',
        'title'         => __( 'Options', 'fusion-framework' ),
        'post_types'    => array( 'price' ),
        //'page_template' => array('page-test.php'), // Visibility of box based on page template selector
        //'post_format' => array('image'), // Visibility of box based on post format
        'position'      => 'side', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low - Priorities of placement
        'sections'      => $boxSections,
    );

    return $metaboxes;
}

add_action("redux/metaboxes/{$redux_opt_name}/boxes", "price_metaboxes");

?>