<?php

/** PAGE **/
function price_page_metaboxes($metaboxes) {
    // Declare your sections
    $boxSections = array();
    $boxSection[] = array(
        //'title'         => __('General Settings', 'redux-framework-demo'),
        //'icon'          => 'el-icon-home', // Only used with metabox position normal or advanced
        'fields'        => array(
            array(
                'id'    => 'opt-info-success',
                'type'  => 'info',
                'style' => 'success',
                //'icon'  => 'el-icon-info-sign',
                'title' => __('Please note', 'redux-framework-demo'),
                'desc'  => __('That if you would like to use shortcodes on your page you will need to paste them into the visual editor above, and enable the "Content" block in the layout manager below.', 'redux-framework-demo')
            ),            
            array(
				'id'=>'Extra-content',
				'type' => 'editor',
				'title' => __('Extra content', 'redux-framework'), 
				//'subtitle' => __('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'redux-framework'),
				//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
			),
	        array(
                "id" => "Page-layout",
                "type" => "sorter",
                "title" => "Layout Manager",
                "subtitle" => "Organise how you want the layout to appear on the this page.",
                //"desc" => "Organise how you want the layout to appear on the this page",
                //"compiler"=>'true',
                //'required' => array('layout','=','1'),       
                'options' => array(
                    "enabled" => array(
                        'content'		=> 'Content',
                    ),
                    "disabled" => array(
                        'prices'		=> 'Prices',
                        'team'			=> 'Team',
                        'services'      => 'Services',
                        'gallery'		=> 'Gallery',
                        'featured'		=> 'Featured pet',
                        'featured-gallery'		=> 'Featured gallery',
                        'testimonials'	=> 'Testimonials',
                        'adoption'		=> 'Adoption short',
                        'adoption-full'      => 'Adoption full',
                        'quote'			=> 'Quote',
                        'extracontent'	=> 'Extra Content',
                        'map'	=> 'Map',
                    ),
                    "backup" => array(
                    ),
                ),
            ),
        ), 
    );

    // Declare your metaboxes
    if (!isset($metaboxes) && !is_array($metaboxes)) {$metaboxes = array(); }
    $metaboxes[] = array(
        'id'            => 'team',
        'title'         => __( 'Options', 'fusion-framework' ),
        'post_types'    => array( 'page' ),
        //'page_template' => array('PricePage.php'), // Visibility of box based on page template selector
        //'post_format' => array('image'), // Visibility of box based on post format
        'position'      => 'advanced', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low - Priorities of placement
        'sections'      => $boxSection,
    );

    return $metaboxes;
}

add_action("redux/metaboxes/{$redux_opt_name}/boxes", "price_page_metaboxes");

?>