<?php 
/**
 * Puppies custom post type
 *
 */
?>

<?php
add_action('init', 'Puppies');
function puppies() {
	register_post_type('Puppies', array(
		'labels' => array(
			'name' => 'Puppies',
			'singular_name' => 'Puppies',
			'add_new' => 'Add new Puppy',
			'edit_item' => 'Edit Puppy',
			'new_item' => 'New Puppy',
			'view_item' => 'View Puppy',
			'search_items' => 'Search Puppies',
			'not_found' => 'No Puppies found',
			'not_found_in_trash' => 'No Puppies found in Trash',
		),
		'public' => true,
		'rewrite' => array('slug' => 'puppies'),
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields', 'page-attributes'),
		'has_archive' => true,
	));
}



/**
 * Show Ring custom post type
 *
 */
add_action('init', 'ShowRing');
function showring() {
	register_post_type('Show Ring', array(		
		'labels' => array(		
			'name' => 'Show Ring',			
			'singular_name' => 'Show Ring',			
			'add_new' => 'Add new Show Ring',			
			'edit_item' => 'Edit Show Ring',			
			'new_item' => 'New Show Ring',			
			'view_item' => 'View Show Ring',			
			'search_items' => 'Search Show Ring',			
			'not_found' => 'No Show Ring found',			
			'not_found_in_trash' => 'No Show Ring found in Trash',
		
		),
		'has_archive' => true,		
		'public' => true,		
		'rewrite' => array('slug' => 'showring'),		
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields')	
	));
}
add_action('admin_head-nav-menus.php', 'wpclean_add_metabox_menu_posttype_archive');

function wpclean_add_metabox_menu_posttype_archive() {
add_meta_box('wpclean-metabox-nav-menu-posttype', 'Custom Post Type Archives', 'wpclean_metabox_menu_posttype_archive', 'nav-menus', 'side', 'default');
}

function wpclean_metabox_menu_posttype_archive() {
$post_types = get_post_types(array('show_in_nav_menus' => true, 'has_archive' => true), 'object');

if ($post_types) :
    $items = array();
    $loop_index = 999999;

    foreach ($post_types as $post_type) {
        $item = new stdClass();
        $loop_index++;

        $item->object_id = $loop_index;
        $item->db_id = 0;
        $item->object = 'post_type_' . $post_type->query_var;
        $item->menu_item_parent = 0;
        $item->type = 'custom';
        $item->title = $post_type->labels->name;
        $item->url = get_post_type_archive_link($post_type->query_var);
        $item->target = '';
        $item->attr_title = '';
        $item->classes = array();
        $item->xfn = '';

        $items[] = $item;
    }

    $walker = new Walker_Nav_Menu_Checklist(array());

    echo '<div id="posttype-archive" class="posttypediv">';
    echo '<div id="tabs-panel-posttype-archive" class="tabs-panel tabs-panel-active">';
    echo '<ul id="posttype-archive-checklist" class="categorychecklist form-no-clear">';
    echo walk_nav_menu_tree(array_map('wp_setup_nav_menu_item', $items), 0, (object) array('walker' => $walker));
    echo '</ul>';
    echo '</div>';
    echo '</div>';

    echo '<p class="button-controls">';
    echo '<span class="add-to-menu">';
    echo '<input type="submit"' . disabled(1, 0) . ' class="button-secondary submit-add-to-menu right" value="' . __('Add to Menu', 'andromedamedia') . '" name="add-posttype-archive-menu-item" id="submit-posttype-archive" />';
    echo '<span class="spinner"></span>';
    echo '</span>';
    echo '</p>';

endif;
}
?>
<?php 
function add_custom_meta_box()
{
	add_meta_box("demo-meta-box", "Choose Puppy", "custom_meta_box_markup", "puppies", "side", "high", null);
}
add_action("add_meta_boxes", "add_custom_meta_box");

function custom_meta_box_markup($object)
{
	wp_nonce_field(basename(__FILE__), "meta-box-nonce");
 	
	$post_type=$_REQUEST['post_type'];

	$mydb = new wpdb('root','','dogspot','localhost');
	
	$rows = $mydb->get_results("select name,animalid from name");
	
	$html='';
	$html.='<form name="pupyfrm" id="pupyfrm">'.
			'<select name="pupy" id="puppy">';
			foreach ($rows as $row) :
				$html.='<option value="'.$row->animalid.'">'.$row->name.'</option>';
			endforeach;
			
		$html.='</select>'.
		'<input type="hidden" id="post_type" name="post_type" value="'.$post_type.'" />'.
		'<input type="button" id="submits" name="submits" value="Get Info" />'.
	'</form>';
	
	echo $html;
	?>
	<script type="text/javascript">
		jQuery(document).ready(function(e) {
			console.log("on load");
			jQuery('body').on('click','#submits',function(){
				
				console.log("on button click")
				var url = "<?php echo get_template_directory_uri();?>/petcreate.php";
				var puppy=jQuery("#puppy");
				var puppy_id = jQuery("#puppy").val();
				var post_type = jQuery("#post_type").val();
				jQuery.ajax({
					url: url,
					type: "POST",
					//dataType:"text",
					dataType:"JSON",
					data: {
						puppy_id:puppy_id,
						post_type:post_type
					},
					success: function(result) {
						console.log(result);						
						jQuery("#title").val(result.name);
						jQuery("#acf-field-short_name").val(result.shortname);
						jQuery("#content").val(result.description);
						jQuery("#acf-field-sex").val(result.sex);
						jQuery("#acf-field-color").val(result.color);
						jQuery(".acf-date_picker").find('input').val(result.dob);
						jQuery("#acf-field-price").val(result.price);
						//jQuery("#set-post-thumbnail").src(result.file_path);
						//jQuery(".hide-if-no-js").find('img').src(result.file_path);
						
						
					},
					error: function(e) {
					}
				});			
			});  
		});
	</script>
<?php } ?>