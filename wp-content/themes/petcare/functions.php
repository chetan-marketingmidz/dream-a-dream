<?php
/*

Petcare Theme Functions

*/

// Due to a limitations of variables and functions, you must globally define
// variables inside functions.
global $petcare_option; // This is your opt_name.

if ( ! isset( $content_width ) ) $content_width = 900;

/**
 * Enqueue web fonts.
 */
function petcare_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'petcare-lato', "$protocol://fonts.googleapis.com/css?family=Lato&subset=all" );
    wp_enqueue_style( 'petcare-cabin', "$protocol://fonts.googleapis.com/css?family=Cabin&subset=all" );
}
add_action( 'wp_enqueue_scripts', 'petcare_fonts' );

// Load the TGM init if it exists
if (file_exists(dirname(__FILE__).'/tgm/tgm-init.php')) {
    require_once( dirname(__FILE__).'/tgm/tgm-init.php' );
}
//include('custom-functions.php');

// Load Redux extensions - MUST be loaded before your options are set
if (file_exists(dirname(__FILE__).'/redux-extensions/extensions-init.php')) {
    require_once( dirname(__FILE__).'/redux-extensions/extensions-init.php' );
} 
    
// Load the embedded Redux Framework
if (file_exists(dirname(__FILE__).'/redux-framework/ReduxCore/framework.php')) {
    require_once( dirname(__FILE__).'/redux-framework/ReduxCore/framework.php' );
} 

if (file_exists(dirname(__FILE__).'/redux-extensions/extensions/metaboxes/loader.php')) {
	require_once( dirname(__FILE__).'/redux-extensions/extensions/metaboxes/loader.php' );
}

if (file_exists(dirname(__FILE__).'/redux-extensions/extensions/metaboxes/config.php')) {
	require_once( dirname(__FILE__).'/redux-extensions/extensions/metaboxes/config.php' ); 
}
 
// Load the theme/plugin options
if (file_exists(dirname(__FILE__).'/options-init.php')) {
    require_once( dirname(__FILE__).'/options-init.php' );
}

/*if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/sample/sample-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/ReduxFramework/sample/sample-config.php' ); 
}*/

 /**
 * Setup Theme Functions
 *
 */
add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('custom-header');
add_theme_support( 'custom-background');


// Register Custom Navigation Walker
require_once('includes/wp_bootstrap_navwalker.php');


/**
 * Define post thumbnail size.
 * Add two additional image sizes.
 *
 */
 
function petcare_images() {

    set_post_thumbnail_size(260, 180); // 260px wide x 180px high
    add_image_size('bootstrap-small', 300, 200); // 300px wide x 200px high
    add_image_size('bootstrap-medium', 360, 270); // 360px wide by 270px high
}

/**
 * Load CSS styles for theme.
 *
 */

function petcare_styles() {
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.1.0', 'all');
	wp_enqueue_style('main-style', get_template_directory_uri() . '/css/style.php', false, '1.0', 'all');
	wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css', false, '1.0', 'all');
}

add_action( 'wp_enqueue_scripts', 'petcare_styles' );

function petcare_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'after_setup_theme', 'petcare_add_editor_styles' );

/**
 * Load JavaScript and jQuery files for theme.
 *
 */
 
function petcare_scripts_loader() {

	wp_enqueue_script('jQuery', get_template_directory_uri() . '/js/jQuery.min.js', false,'2.1.0',true);
	wp_enqueue_script('pace', get_template_directory_uri() . '/js/pace.min.js', false,'0.5.1',true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', false, '3.1.0', true);
    wp_enqueue_script('caroufredsel', get_template_directory_uri() . '/js/carouFredSel.js', false,'6.2.0',true);
    wp_enqueue_script('lightbox', get_template_directory_uri() . '/js/ekkoLightbox.js', false,'',true);
    wp_enqueue_script('map', 'http://maps.google.com/maps/api/js?sensor=false&amp;language=en', false,'5.1.1',true);
    wp_enqueue_script('gmap', get_template_directory_uri() . '/js/gmap3.min.js', false,'5.1.1',true);
    wp_enqueue_script('stellar', get_template_directory_uri() . '/js/jquery.stellar.min.js', false,'0.6.2',true);
    wp_enqueue_script('bxslider', get_template_directory_uri() . '/js/jquery.bxslider.min.js', false,'4.1.',true);
    wp_enqueue_script('ui', get_template_directory_uri() . '/js/jquery-ui.min.js', false,'1.10.4',true);
    wp_enqueue_script('wow', get_template_directory_uri() . '/js/wow.js', false,'1.10.4',true);
    wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.js', false,'1.0',true);
    wp_enqueue_script('caal_map', get_template_directory_uri() . '/js/map.php', false,'1.0',true);

}
add_action('wp_enqueue_scripts', 'petcare_scripts_loader');

/**
 * Define theme's widget areas.
 *
 */
function petcare_widgets_init() {

    register_sidebar(
        array(
            'name'          => __('Blog Sidebar', 'petcare'),
            'id'            => 'blog-page',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => "</div>",
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );

}
add_action('init', 'petcare_widgets_init');

/**
 * Register menu
 *
 */
 
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'PetCare' ),
));


/**
 * Display page next/previous navigation links.
 *
 */
 
if (!function_exists('petcare_content_nav')):
    function petcare_content_nav($nav_id) {

        global $wp_query, $post;

        if ($wp_query->max_num_pages > 1) : ?>

        <nav id="<?php echo $nav_id; ?>" class="navigation" role="navigation">
            <h3 class="assistive-text"><?php _e('Post navigation', 'petcare'); ?></h3>
            <div class="nav-previous alignleft"><?php next_posts_link(
                __('<span class="meta-nav">&larr;</span> Older posts', 'petcare')
            ); ?></div>
            <div class="nav-next alignright"><?php previous_posts_link(
                __('Newer posts <span class="meta-nav">&rarr;</span>', 'petcare')
            ); ?></div>
        </nav><!-- #<?php echo $nav_id; ?> .navigation -->

        <?php endif;
    }
endif;

/**
 * Custom menu icons
 *
 */
 
function add_menu_icons_styles(){
?>
 
<style>
#adminmenu .menu-icon-gallery div.wp-menu-image:before {
  content: "\f128";
}
#adminmenu .menu-icon-service div.wp-menu-image:before {
  content: "\f313";
}
#adminmenu .menu-icon-hero div.wp-menu-image:before {
  content: "\f472";
}
#adminmenu .menu-icon-adoption div.wp-menu-image:before {
  content: "\f133";
}
#adminmenu .menu-icon-testimonials div.wp-menu-image:before {
  content: "\f125";
}

#adminmenu .menu-icon-team div.wp-menu-image:before {
  content: "\f307";
}

#adminmenu .menu-icon-price div.wp-menu-image:before {
  content: "\f323";
}
</style>
 
<?php
}
add_action( 'admin_head', 'add_menu_icons_styles' );

/**
 * Hero Slider custom post type
 *
 */

add_action( 'init', 'register_cpt_hero' );

function register_cpt_hero() {

    $labels = array( 
        'name' => _x( 'Hero slider', 'hero', 'petcare' ),
        'singular_name' => _x( 'Hero', 'hero', 'petcare' ),
        'add_new' => _x( 'Add slide', 'hero', 'petcare' ),
        'add_new_item' => _x( 'Add slide', 'hero', 'petcare' ),
        'edit_item' => _x( 'Edit slide', 'hero', 'petcare' ),
        'new_item' => _x( 'New slide', 'hero', 'petcare' ),
        'view_item' => _x( 'View slide', 'hero', 'petcare' ),
        'search_items' => _x( 'Search slides', 'hero', 'petcare' ),
        'not_found' => _x( 'No slides found', 'hero', 'petcare' ),
        'not_found_in_trash' => _x( 'No slides found in Trash', 'hero', 'petcare' ),
        'parent_item_colon' => _x( 'Parent slide:', 'hero', 'petcare' ),
        'menu_name' => _x( 'Hero slider', 'hero', 'petcare' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Create heroslide',
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 14,
        'menu_icon' => '',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );

    register_post_type( 'hero', $args );
}

/**
 * Gallery custom post type
 *
 */

add_action( 'init', 'petcare_gallery' );

function petcare_gallery() {

    $labels = array( 
        'name' => _x( 'Gallery', 'gallery', 'petcare' ),
        'singular_name' => _x( 'gallery', 'gallery', 'petcare' ),
        'add_new' => _x( 'Add image', 'gallery', 'petcare' ),
        'add_new_item' => _x( 'Add image', 'gallery', 'petcare' ),
        'edit_item' => _x( 'Edit image', 'gallery', 'petcare' ),
        'new_item' => _x( 'New image', 'gallery', 'petcare' ),
        'view_item' => _x( 'View images', 'gallery', 'petcare' ),
        'search_items' => _x( 'Search images', 'gallery', 'petcare' ),
        'not_found' => _x( 'No images found', 'gallery', 'petcare' ),
        'not_found_in_trash' => _x( 'No images found in Trash', 'gallery', 'petcare' ),
        'parent_item_colon' => _x( 'Parent image:', 'gallery', 'petcare' ),
        'menu_name' => _x( 'Gallery', 'gallery', 'petcare' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Create gallery images',
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 17,
        'menu_icon' => '',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );

    register_post_type( 'gallery', $args );
}

add_image_size( 'Gallery', 374, 9999 );

add_theme_support('post-thumbnails');
add_image_size('featured_preview', 100, 100, true);

// GET FEATURED IMAGE
function ST4_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
        return $post_thumbnail_img[0];
    }
}

// ADD NEW COLUMN
function ST4_columns_head($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = ST4_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" />';
        }
    }
}

add_filter('manage_posts_columns', 'ST4_columns_head');
add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2);



/**
 * Adoption custom post type
 *
 */

add_action( 'init', 'petcare_adoption' );

function petcare_adoption() {

    $labels = array( 
        'name' => _x( 'Adoption', 'adoption', 'petcare' ),
        'singular_name' => _x( 'gallery', 'adoption', 'petcare' ),
        'add_new' => _x( 'Add new', 'adoption', 'petcare' ),
        'add_new_item' => _x( 'Add new', 'adoption', 'petcare' ),
        'edit_item' => _x( 'Edit post', 'adoption', 'petcare' ),
        'new_item' => _x( 'New post', 'adoption', 'petcare' ),
        'view_item' => _x( 'View posts', 'adoption', 'petcare' ),
        'search_items' => _x( 'Search posts', 'adoption', 'petcare' ),
        'not_found' => _x( 'No posts found', 'adoption', 'petcare' ),
        'not_found_in_trash' => _x( 'No posts found in Trash', 'gallery', 'petcare' ),
        'parent_item_colon' => _x( 'Parent image:', 'adoption', 'petcare' ),
        'menu_name' => _x( 'Adoption', 'adoption', 'petcare' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => ' Create adoption posts',
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 19,
        'menu_icon' => '',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );

    register_post_type( 'adoption', $args );
}

add_image_size( 'Adoption', 360, 9999 );

/**
 * Services custom post type
 *
 */

add_action( 'init', 'petcare_services' );

function petcare_services() {

    $labels = array( 
        'name' => _x( 'Services', 'service', 'petcare' ),
        'singular_name' => _x( 'service', 'service', 'petcare' ),
        'add_new' => _x( 'Add service', 'service', 'petcare' ),
        'add_new_item' => _x( 'Add service', 'service', 'petcare' ),
        'edit_item' => _x( 'Edit service', 'service', 'petcare' ),
        'new_item' => _x( 'New service', 'service', 'petcare' ),
        'view_item' => _x( 'View services', 'service', 'petcare' ),
        'search_items' => _x( 'Search services', 'service', 'petcare' ),
        'not_found' => _x( 'No services found', 'service', 'petcare' ),
        'not_found_in_trash' => _x( 'No services found in Trash', 'service', 'petcare' ),
        'parent_item_colon' => _x( 'Parent service:', 'service', 'petcare' ),
        'menu_name' => _x( 'Services', 'service', 'petcare' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Create pricng tables',
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 16,
        'menu_icon' => '',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );

    register_post_type( 'service', $args );
}

/**
 * Services custom post type
 *
 */

add_action( 'init', 'petcare_team' );

function petcare_team() {

    $labels = array( 
        'name' => _x( 'Team', 'team', 'petcare' ),
        'singular_name' => _x( 'Team', 'team', 'petcare' ),
        'add_new' => _x( 'Add team member', 'team', 'petcare' ),
        'add_new_item' => _x( 'Add team member', 'team', 'petcare' ),
        'edit_item' => _x( 'Edit team member', 'team', 'petcare' ),
        'new_item' => _x( 'New team member', 'team', 'petcare' ),
        'view_item' => _x( 'View team members', 'team', 'petcare' ),
        'search_items' => _x( 'Search team members', 'team', 'petcare' ),
        'not_found' => _x( 'No team members found', 'team', 'petcare' ),
        'not_found_in_trash' => _x( 'No steam members found in Trash', 'team', 'petcare' ),
        'parent_item_colon' => _x( 'Parent team:', 'team', 'petcare' ),
        'menu_name' => _x( 'Team', 'team', 'petcare' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Create team members',
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 16,
        'menu_icon' => '',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );

    register_post_type( 'team', $args );
}

/**
 * Team members custom post type
 *
 */

add_action( 'init', 'petcare_pricing' );

function petcare_pricing() {

    $labels = array( 
        'name' => _x( 'Prices', 'price', 'inkd' ),
        'singular_name' => _x( 'Price', 'price', 'inkd' ),
        'add_new' => _x( 'Add price', 'price', 'inkd' ),
        'add_new_item' => _x( 'Add price', 'price', 'inkd' ),
        'edit_item' => _x( 'Edit price', 'price', 'inkd' ),
        'new_item' => _x( 'New price', 'price', 'inkd' ),
        'view_item' => _x( 'View prices', 'price', 'inkd' ),
        'search_items' => _x( 'Search prices', 'price', 'inkd' ),
        'not_found' => _x( 'No prices found', 'price', 'inkd' ),
        'not_found_in_trash' => _x( 'No prices found in Trash', 'price', 'inkd' ),
        'parent_item_colon' => _x( 'Parent price:', 'price', 'inkd' ),
        'menu_name' => _x( 'Prices', 'price', 'inkd' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Create pricng tables',
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 22,
        'menu_icon' => '',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );

    register_post_type( 'price', $args );
}

/**
 * Testimonials custom post type
 *
 */

add_action( 'init', 'petcare_testimonials' );

function petcare_testimonials() {

    $labels = array( 
        'name' => _x( 'Testimonials', 'testimonials', 'petcare' ),
        'singular_name' => _x( 'Testimonial', 'testimonials', 'petcare' ),
        'add_new' => _x( 'Add testimonial', 'testimonials', 'petcare' ),
        'add_new_item' => _x( 'Add testimonial', 'testimonials', 'petcare' ),
        'edit_item' => _x( 'Edit testimonials', 'testimonials', 'petcare' ),
        'new_item' => _x( 'New testimonial', 'testimonials', 'petcare' ),
        'view_item' => _x( 'View testimonials', 'testimonials', 'petcare' ),
        'search_items' => _x( 'Search testimonials', 'testimonials', 'petcare' ),
        'not_found' => _x( 'No testimonials found', 'testimonials', 'petcare' ),
        'not_found_in_trash' => _x( 'No testimonials found in Trash', 'testimonials', 'petcare' ),
        'parent_item_colon' => _x( 'Parent testimonial:', 'testimonials', 'petcare' ),
        'menu_name' => _x( 'Testimonials', 'testimonials', 'petcare' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Add testimonials',
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 18,
        'menu_icon' => '',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );

    register_post_type( 'testimonials', $args );
}


/**
 * Post pagination
 *
 */
function numeric_posts_pagination() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div id="pagination"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>?</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>?</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}

/**
 * Comments
 *
 */
function petcare_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'div';
		$add_below = 'div-comment';
	}
?>
	<<?php echo $tag ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="row comment">
	<?php endif; ?>
	<div class="comment-author vcard col-md-1">
		<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
	</div>
	<?php if ( $comment->comment_approved == '0' ) : ?>
		<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'petcare' ); ?></em>
		<br />
	<?php endif; ?>

	<div class="comment-meta commentmetadata col-md-11">
		<h4><?php printf( __( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); ?></h4>
		<?php comment_text(); ?>
		<p class="comment-date"><?php printf( __('%1$s at %2$s', 'petcare'), get_comment_date(),  get_comment_time() ); ?></p>
	<div class="reply">
		<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?> <?php edit_comment_link( __( '(Edit)', 'petcare' ), '  ', '' );?>
	</div>
	</div>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php
}


/**
 * Register footer widgets
 *
 */
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> 'Footer column 1',
		'description' => __( "Please include widgets for the first column in the footer i.e an text block about your company." ),
		'id' => 'ft_col_1',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	));
	register_sidebar(array(
		'name'=> 'Footer column 2',
		'description' => __( "Please include widgets for the second column in the footer i.e a recent posts widget." ),
		'id' => 'ft_col_2',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	));
	register_sidebar(array(
		'name'=> 'Footer column 3',
		'description' => __( "Please include widgets for the third column in the footer i.e a footer navigation." ),
		'id' => 'ft_col_3',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	));
	register_sidebar(array(
		'name'=> 'Footer column 4',
		'description' => __( "Please include widgets for the fourth column in the footer i.e a description for your contact info." ),
		'id' => 'ft_col_4',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	));
}

include(TEMPLATEPATH . '/shortcodes.php');

add_filter('next_posts_link_attributes', 'posts_link_attributes_1');
add_filter('previous_posts_link_attributes', 'posts_link_attributes_2');

function posts_link_attributes_1() {
    return 'class="prev-post"';
}
function posts_link_attributes_2() {
    return 'class="next-post"';
}

/**
 * Comments
 *
 */
function inkd_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'div';
		$add_below = 'div-comment';
	}
?>
	<<?php echo $tag ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="row comment">
	<?php endif; ?>
	<div class="comment-author vcard col-md-2 wow rubberBand" data-wow-offset="150">
		<?php echo get_avatar( $comment, 132 ); ?>
	</div>
	<?php if ( $comment->comment_approved == '0' ) : ?>
		<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'inkd' ); ?></em>
		<br />
	<?php endif; ?>

	<div class="comment-meta commentmetadata col-md-10">
		<h4><?php printf( __( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); ?></h4>
		<?php comment_text(); ?>
		<p class="comment-date"><em><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' ago'; ?></em> <span>|</span> <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?> <?php edit_comment_link( __( '(Edit)', 'inkd' ), '  ', '' );?></p>
	</div>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php
}

include(TEMPLATEPATH . '/bio.php');

// 1. Enqueue my admin script
//function add_my_admin_script(){
//    wp_enqueue_script('admin_script', get_template_directory_uri() . '/js/admin_script.js', array('jquery'));
//    //
//    wp_localize_script( 'admin_script', 'ajax_object',
//        array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => 1234 ) );
//}
//add_action('admin_enqueue_scripts', 'add_my_admin_script');   

// 2. The Function to check Post Titles
/************/
// Check Titles
/************/
//add_action( 'wp_ajax_my_action', 'my_action_callback' );
//
//function my_action_callback() {
//    global $wpdb;
//    $title_exists = $wpdb->get_results( 
//        "SELECT ID FROM $wpdb->posts WHERE post_title LIKE '" . $_POST['this_convidado_title'] . "' AND post_type = '" . $_POST['post_type'] . "'"
//    );
//    if($_POST['post_ID'] != ""){
//        foreach ($title_exists as $key => $this_id) {
//            if($_POST['post_ID'] == $this_id->ID){
//                $this_is_the_post = $this_id->ID;
//            }
//        }
//    }
//    if($this_is_the_post){
//        echo (count($title_exists)-1);
//    } else {
//        echo count($title_exists);
//    }
//    die('sady how r uu');
//}

///////////////////// publish post hook
//add_action ( 'publish_post', 'duplicatepost' );
//function duplicatepost() {
	//global $wpdb;
//    $title_exists = $wpdb->get_results( "SELECT a.ID, a.post_title, a.post_type, a.post_status FROM wp_posts AS a
//   INNER JOIN (
//      SELECT post_title, MIN( id ) AS min_id
//      FROM wp_posts
//      WHERE post_type = 'puppies'
//      AND post_status = 'publish'
//      GROUP BY post_title
//      HAVING COUNT( * ) > 1
//   ) AS b ON b.post_title = a.post_title
//AND b.min_id <> a.id
//AND a.post_type = 'puppies'
//AND a.post_status = 'publish' "	
//);/
//}

?>