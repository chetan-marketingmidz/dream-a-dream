<?php
/*

Template Name: Home Template
 
*/
 
get_header(); ?>

<?php

$layout = $petcare_option['opt-homepage-layout']['enabled'];

if ($layout): foreach ($layout as $key=>$value) {

    switch($key) {
	    
	    case 'nav': get_template_part( 'templates/content', 'Nav' );
        include(dirname(__FILE__) . '/../includes/nav.php');
        break;
        
        case 'hero': get_template_part( 'templates/content', 'Hero' );
        include(dirname(__FILE__) . '/../includes/hero.php');
        break;
        
        case 'services': get_template_part( 'templates/content', 'Services' );
        include(dirname(__FILE__) . '/../includes/services.php');
        break;
        
        case 'gallery': get_template_part( 'templates/content', 'Gallery' );
        include(dirname(__FILE__) . '/../includes/gallery.php');
        break;
        
        case 'featured': get_template_part( 'templates/content', 'Featured animal' );
        include(dirname(__FILE__) . '/../includes/featured.php');
        break;
        
        case 'testimonials': get_template_part( 'templates/content', 'Testimonials' );
        include(dirname(__FILE__) . '/../includes/testimonials.php');
        break;
        
        case 'adoption': get_template_part( 'templates/content', 'Adoption' );
        //include(dirname(__FILE__) . '/../includes/adoption.php');
        break;
		
        
        case 'quote': get_template_part( 'templates/content', 'Quote' );
        include(dirname(__FILE__) . '/../includes/quote.php');
        break;
    
    }

}

endif;

?>

<?php get_footer(); ?>