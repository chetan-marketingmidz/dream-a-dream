<?php
/*

Columns shortcodes

*/
 
function petcare_shortcode_adopt_feat( $atts, $content = null ) {
    extract(shortcode_atts(array(
	'title'  		=> ''
    ), $atts));

	$title = ($title) ? ''.$title : '';
	
	return '<li><span>' .$title. '</span>' .do_shortcode($content). '</li>';
	
}
add_shortcode('feature', 'petcare_shortcode_adopt_feat');

function petcare_shortcode_features($params, $content = null) {
	
	return
	'<ol>' . do_shortcode($content) . '</ol>';

}
add_shortcode('features','petcare_shortcode_features');

/**
 * Columns shortcodes
 *
 */

function petcare_shortcode_row($params, $content = null) {
	
	return
	'<div class="row">' . do_shortcode($content) . '</div>';

}
add_shortcode('row','petcare_shortcode_row');

function petcare_shortcode_column( $atts, $content = null ) {
    extract(shortcode_atts(array(
	'size'  		=> '',
	'align'			=> '',
	'background'	=> '',
    ), $atts));

	$size = ($size) ? 'col-md-'.$size : '';
	$align = ($align) ? ' align'.$align : '';
	$background = ($background) ? ' '.$background : '';
	
	
	if (strpos($size, "last") === false) {
		return '<div class="' .$size.$align.$background. ' columns-shortcode">' .do_shortcode($content). '</div>';
	}
	
}
add_shortcode('column', 'petcare_shortcode_column');

/**
 * Accordion shortcodes
 *
 */

function petcare_shortcode_accordion_wrap($params, $content = null) {
	
	return
	'<div class="accordion">' . do_shortcode($content) . '</div>';

}
add_shortcode('accordion-wrap','petcare_shortcode_accordion_wrap');

function petcare_shortcode_accordion( $atts, $content = null ) {
    extract(shortcode_atts(array(
	'title'  		=> '',
	'align'			=> ''
    ), $atts));

	$title = ($title) ? ''.$title : '';
	$align = ($align) ? ' align'.$align : '';
	
	return '<h4 class="' .$align. '">' .$title. '</h4> <div class="' .$align. '">' .do_shortcode($content). '</div>';
	
}
add_shortcode('accordion', 'petcare_shortcode_accordion');

/**
 * Time shortcodes
 *
 */

function petcare_shortcode_time_wrap($params, $content = null) {
	
	return
	'<div class="hours">' . do_shortcode($content) . '</div>';

}
add_shortcode('time-wrap','petcare_shortcode_time_wrap');

function petcare_shortcode_time( $atts, $content = null ) {
    extract(shortcode_atts(array(
	'title'  		=> '',
	'time'			=> ''
    ), $atts));

	$title = ($title) ? ''.$title : '';
	
	return '<div>
				<p class="day"><strong>' .$title. '</strong></p>
				<p class="time">' .$time. '</p>
			</div>';
	
}
add_shortcode('time', 'petcare_shortcode_time');

/**
 * Tab shortcodes
 *
 */

function petcare_shortcode_tabs_wrap($params, $content = null) {
	
	return
	'<div class="tabs">' . do_shortcode($content) . '</div>';

}
add_shortcode('tabs-wrap','petcare_shortcode_tabs_wrap');

function petcare_shortcode_tabs_nav_wrap($params, $content = null) {
	
	return
	'<ul>' . do_shortcode($content) . '</ul>';

}
add_shortcode('tabs-nav-wrap','petcare_shortcode_tabs_nav_wrap');

function petcare_shortcode_tab_nav( $atts, $content = null ) {
    extract(shortcode_atts(array(
	'title'		=> '',
	'id'  		=> '',
	'align'		=> ''
    ), $atts));

	$id = ($id) ? ''.$id : '';
	$align = ($align) ? ' align'.$align : '';
	
	return '<li><a href="#tab-' .$id. '">' .$title. '</a>' .do_shortcode($content). '</li>';
	
}
add_shortcode('tabs-nav', 'petcare_shortcode_tab_nav');

function petcare_shortcode_tab( $atts, $content = null ) {
    extract(shortcode_atts(array(
	'id'  		=> '',
	'align'			=> ''
    ), $atts));

	$id = ($id) ? ''.$id : '';
	$align = ($align) ? ' align'.$align : '';
	
	return '<div id="tab-' .$id. '" class="' .$align. '">' .do_shortcode($content). '</div>';
	
}
add_shortcode('tab', 'petcare_shortcode_tab');

function petcare_shortcode_h3( $atts, $content = null ) {
    extract(shortcode_atts(array(
	'align'			=> ''
    ), $atts));

	$align = ($align) ? ' align'.$align : '';
	
	return '<h3 class="border' .$align. '"><span>' .do_shortcode($content). '</span></h3>';
	
}
add_shortcode('h3', 'petcare_shortcode_h3');

// clean up formatting in shortcodes
function petcare_clean_shortcodes($content) {   
	$array = array (
		'<p>[' => '[', 
		']</p>' => ']', 
		']<br />' => ']'
	);

	$content = strtr($content, $array);
	return $content;
}
add_filter('the_content', 'petcare_clean_shortcodes');

?>