<?php
/*

The template for displaying Archive pages.

*/
get_header();

include(TEMPLATEPATH . '/includes/inner-nav.php'); ?>
<!-- Blog -->

<!-- Title -->
<div class="title" id="top" data-stellar-background-ratio=".5">
	<div class="container">
		<h1 class="archive-title" data-stellar-ratio="2" data-stellar-vertical-offset="0" data-stellar-horizontal-offset="0"><?php
                        if (is_day()) {
                            printf(__('Daily Archives: %s', 'bootstrapwp'), '<span>' . get_the_date() . '</span>');
                        } elseif (is_month()) {
                            printf(
                                __('Monthly Archives: %s', 'bootstrapwp'),
                                '<span>' . get_the_date(_x('F Y', 'monthly archives date format', 'bootstrapwp')) . '</span>'
                            );
                        } elseif (is_year()) {
                            printf(
                                __('Yearly Archives: %s', 'bootstrapwp'),
                                '<span>' . get_the_date(_x('Y', 'yearly archives date format', 'bootstrapwp')) . '</span>'
                            );
                        } elseif (is_tag()) {
                            printf(__('Tag Archives: %s', 'bootstrapwp'), '<span>' . single_tag_title('', false) . '</span>');
                            // Show an optional tag description
                            $tag_description = tag_description();
                            if ($tag_description) {
                                echo apply_filters(
                                    'tag_archive_meta',
                                    '<div class="tag-archive-meta">' . $tag_description . '</div>'
                                );
                            }
                        } elseif (is_category()) {
                            printf(
                                __('Category Archives: %s', 'bootstrapwp'),
                                '<span>' . single_cat_title('', false) . '</span>'
                            );
                            // Show an optional category description
                            $category_description = category_description();
                            if ($category_description) {
                                echo apply_filters(
                                    'category_archive_meta',
                                    '<div class="category-archive-meta">' . $category_description . '</div>'
                                );
                            }
                        } else {
                            _e('Blog Archives', 'bootstrapwp');
                        }
                        ?></h1>
	</div>
</div>
<!-- Title end -->

<!-- Blog -->
<div class="container blog-holder">
	<div class="row">
		<div class="col-md-9 posts">
			<div class="row"><div class="col-md-12">
				<?php if (have_posts()) : while (have_posts()) : the_post();?>
					<!-- Single post -->
						<div class="col-md-4">
							<div <?php post_class(); ?>>
								
								<?php /*?><p class="post-info">
									<?php the_date("j"); ?> <?php the_time("M"); ?> <span>|</span>
									Posted by <?php the_author(); ?> <span>|</span>
									<a href="<?php comments_link(); ?>" title="<?php comments_number( 'no comments', '1 comment', '% comments' ); ?>"><?php comments_number( 'no comments', '1 comment', '% comments' ); ?></a>  <span>|</span>
									<?php the_tags('', ', ', '<br />'); ?> 
									</p><?php */?>
								<?php if(has_post_thumbnail()) :?>
									<p class="post-img-wrap">
										<a href="<?php the_permalink(); ?>" title="<?php the_title();?>" class="post-image wow bounceIn" data-wow-offset="100">
											<?php the_post_thumbnail(); ?>
										</a>
									</p>
								<?php endif;?>
                                <h2><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><span><?php the_title();?></span></a></h2>
								<?php //the_excerpt(); ?>
							</div>
						</div>
					<!-- Single post end -->
				<?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.', 'inkd'); ?></p>
				<?php endif; ?></div>
			</div>
		</div>
		<div class="col-md-3 sidebar">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<!-- Blog end -->
<!-- Pagination -->
<div class="pagination-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php numeric_posts_pagination(); ?>
			</div>
		</div>
	</div>
</div>
<!-- Pagination end -->
<?php get_footer(); ?>