<?php
/*

Default Post Template
Description: Post template with a content container and right sidebar.

*/
get_header();

include(TEMPLATEPATH . '/includes/inner-nav.php'); ?>
<!-- Blog -->
<div class="container blog-holder">
	<div class="row">
		<div class="col-md-9 posts">
			<div class="row">
				<?php if (have_posts()) : while (have_posts()) : the_post();?>
					<!-- Single post -->
						<div class="col-md-12">
							<div <?php post_class(); ?>>
								<h2><span><?php the_title();?></span></h2>
								<p class="post-info">
									<?php the_date("j"); ?> <?php the_time("M"); ?> <span>|</span>
									Posted by <?php the_author(); ?> <span>|</span>
									<a href="#comments" title="<?php comments_number( 'no comments', '1 comment', '% comments' ); ?>"><?php comments_number( 'no comments', '1 comment', '% comments' ); ?></a>  <span>|</span>
									<?php the_tags('', ', ', '<br />'); ?> 
									</p>
								<?php if(has_post_thumbnail()) :?>
									<p class="post-img-wrap">
										<?php the_post_thumbnail(); ?>
									</p>
								<?php endif;?>
								<?php the_content(); ?>
								<div class="row author-bio">
									<div class="col-md-2 wow rubberBand" data-wow-offset="150">
										<?php echo get_avatar( get_the_author_meta('email') , 132 ); ?>
									</div>
									<div class="col-md-10">
										<p class="author">Posted by<br />
										<span><?php echo get_the_author_meta('nickname'); ?></span>
										<p><?php echo get_the_author_meta('description'); ?></p>
										<p>
											<?php $twitter_profile = get_the_author_meta( 'twitter_profile' );
											if ( $twitter_profile && $twitter_profile != '' ) {
												echo '<a href="' . esc_url($twitter_profile) . '" class="twitter"><img src="' . get_template_directory_uri('template_directory') . '/images/profile-twitter-green.png" alt="Follow ' . get_the_author_meta('nickname') . ' on Twitter" /></a>';
											}
															
											$facebook_profile = get_the_author_meta( 'facebook_profile' );
											if ( $facebook_profile && $facebook_profile != '' ) {
												echo '<a href="' . esc_url($facebook_profile) . '" class="facebook"><img src="' . get_template_directory_uri('template_directory') . '/images/profile-facebook-green.png" alt="Follow ' . get_the_author_meta('nickname') . ' on Facebook" /></a>';
											} ?>
										</p>
									</div>
								</div>
								<?php comments_template(); ?>
							</div>
						</div>
						<!-- Single post end -->
					<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.', 'petcare'); ?></p>
						<?php wp_link_pages(); ?>
					<?php endif; ?>
		
				</div>
			</div>
			<div class="col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>
<!-- Blog end -->

<?php get_footer(); ?>
