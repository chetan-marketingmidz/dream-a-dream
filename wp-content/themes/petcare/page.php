<?php
/*

Template Name: Default Page

*/
get_header(); ?>
<?php include(TEMPLATEPATH . '/includes/inner-nav.php'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12 centered title-margin">
			<h3 class="border"><span><?php the_title(); ?></span></h3>
		</div>
	</div>
</div>
<?php
global $petcare_option;

$layout = $petcare_option['Page-layout'];

if (!empty($layout)){

	echo '<div class="left-align">';	
	
	$layout = $petcare_option['Page-layout']['enabled'];
	if ($layout): foreach ($layout as $key=>$value) {
	
	    switch($key) {
		     
		    case 'content': get_template_part( 'templates/content', 'Content' );
	        include(dirname(__FILE__) . '/includes/content.php');
	        break;
	         
	        case 'prices': get_template_part( 'templates/content', 'Prices' );
	        include(dirname(__FILE__) . '/includes/prices.php');
	        break;

	        case 'services': get_template_part( 'templates/content', 'Services' );
	        include(dirname(__FILE__) . '/includes/full-services.php');
	        break;
	  
		    case 'team': get_template_part( 'templates/content', 'Team' );
	        include(dirname(__FILE__) . '/includes/team.php');
	        break;
	        
	        case 'gallery': get_template_part( 'templates/content', 'Gallery' );
	        include(dirname(__FILE__) . '/includes/gallery.php');
	        break;
	        
	        case 'featured': get_template_part( 'templates/content', 'Featured pet' );
	        include(dirname(__FILE__) . '/includes/featured.php');
	        break;
	        
	        case 'featured-gallery': get_template_part( 'templates/content', 'Featured pet gallery' );
	        include(dirname(__FILE__) . '/includes/adoption-gallery.php');
	        break;
	        
	        case 'testimonials': get_template_part( 'templates/content', 'Testimonials' );
	        include(dirname(__FILE__) . '/includes/testimonials.php');
	        break;
	        
	        case 'adoption': get_template_part( 'templates/content', 'Adoption' );
	        include(dirname(__FILE__) . '/includes/adoption.php');
	        break;
			
			case 'adoption-full': get_template_part( 'templates/content', 'Adoption Full' );
	        include(dirname(__FILE__) . '/includes/adoption-full.php');
	        break;
			
			case 'quote': get_template_part( 'templates/content', 'Quote' );
	        include(dirname(__FILE__) . '/includes/quote.php');
	        break;
	        
	        case 'extracontent': get_template_part( 'templates/content', 'Extra Content' );
	        include(dirname(__FILE__) . '/includes/extra-content.php');
	        break;
	        
	        case 'map': get_template_part( 'templates/content', 'Map' );
	        include(dirname(__FILE__) . '/includes/map.php');
	        break;
	    
	    }
	
	}
	
	endif;
	
	echo '</div>';

} else {
	echo '<div class="left-aligned container">';
		while (have_posts()) : the_post();
			the_content();
		endwhile;
	echo '</div>';
}

get_footer(); ?>