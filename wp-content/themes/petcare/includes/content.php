<div class="container">
	<div class="row">
		<div class="col-md-12 centered remove-margin">
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>