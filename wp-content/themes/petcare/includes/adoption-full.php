<!-- Adoption -->
<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3 class="border"><span><?php echo $petcare_option['Adoption-title']; ?></span></h3>
			<?php echo $petcare_option['Adoption-content']; ?>
		</div>
	</div>
	<div class="row adoption">
	<?php $args = array( 'post_type' => 'Adoption', 'posts_per_page' => -1 );
		  $loop = new WP_Query( $args );
		  while ( $loop->have_posts() ) : $loop->the_post();
	?>
		<div class="col-md-4 wow FlipInX">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail('Adoption'); ?>
			</a>
			<div class="title">
				<h5>
					<span data-hover="<?php the_title(); ?>"><?php the_title(); ?></span>
				</h5>
			</div>
		</div>
	<?php endwhile; ?>
	
</div>
<!-- Adoption end -->