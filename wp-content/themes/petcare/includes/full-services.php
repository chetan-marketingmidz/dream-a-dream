<!-- Services -->
<div class="container service-block">
	<div class="row">
		<?php
		  $args = array(
		  	'post_type' => 'Service',
		  	'post_status' => 'publish',
		  );
		  $loop = new WP_Query( $args );
		  while ( $loop->have_posts() ) : $loop->the_post();
		?>
			<div class="col-md-4 col3">
				<a href="<?php if($petcare_option['ext-service-link']){ echo $petcare_option['ext-service-link']; } else {  the_permalink(); } ?>" title="<?php the_title(); ?>" title="<?php the_title(); ?>" class="roundal wow bounceInUp"><?php the_post_thumbnail(); ?></a>
				<h3><?php the_title(); ?></h3>
				<p><?php echo substr(get_the_excerpt(), 0,150); ?>&hellip;</p>
				<form method="get" action="<?php if($petcare_option['ext-service-link']){ echo $petcare_option['ext-service-link']; } else {  the_permalink(); } ?>">
					<button type="submit" class="btn btn-default btn-green"><?php echo $petcare_option['Button-text']; ?></button>
				</form>
			</div>
		<?php endwhile; ?>
	</div>
</div>
<!-- Services end -->