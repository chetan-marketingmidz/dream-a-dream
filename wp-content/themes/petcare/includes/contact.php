<div class="container content">
	<div class="row">
		<div class="col-md-9" id="contact_form">
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</div>
		<div class="col-md-3">
			<ul class="contact-info">
				<li class="telephone">
					<?php echo $petcare_option['Telephone']; ?>
				</li>
				<li class="address">
					<?php echo $petcare_option['Address']; ?>
				</li>
				<li class="mail">
					<?php echo $petcare_option['EmailAddress']; ?>
				</li>
			</ul>
		</div>
	</div>
</div>