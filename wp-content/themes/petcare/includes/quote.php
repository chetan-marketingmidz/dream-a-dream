<!-- Purchase -->
<div class="purchase">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<p><?php echo $petcare_option['Quote-para']; ?><br />
				<span><?php echo $petcare_option['Quote-subpara']; ?></span></p>
			</div>
			<div class="col-md-3 purchase-button">
				<form method="get" action="<?php echo get_permalink($petcare_option['Quote-link']); ?>">
					<button type="submit" class="btn btn-default btn-green"><?php echo $petcare_option['Quote-buttontxt']; ?></button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Purchase end -->