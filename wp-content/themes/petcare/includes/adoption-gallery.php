<!-- Carousel -->
<div id="c-carousel">
	<div id="wrapper">
		<div class="featured">
			<h3><span>Featured dog</span><br />
			<?php echo $petcare_option['Pet-name']; ?></h3>
			<p><?php echo $petcare_option['Featured-content']; ?></p>
			<form method="get" action="adoption-single.html">
				<button type="submit" class="btn btn-default btn-green">Meet buddy</button>
			</form>
		</div>
		<div id="carousel">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php
					foreach($petcare_option['Feat-slides'] as $slide) {
						echo '<div class="wow flipInX">';
						echo '<a href="' . $slide['url'] . '" title="' . $slide['title'] .'" data-hover="' . $slide['title'] .'">';
				        echo '<img src="' . $slide['image'] . '" alt="' . $slide['description'] . '">';
				        echo '</a>';
				        echo '</div>';
				    }
				?>
			<?php endwhile; endif; ?>
		</div>
		<div id="pager" class="pager"></div>
	</div>
</div>
<!-- Carousel end -->