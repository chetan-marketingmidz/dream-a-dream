<div class="container">	
	<div class="row services-single">
		<div class="col-md-6">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<img src="<?php echo $petcare_option['opt-media']['url']; ?>" alt="<?php the_title(); ?>" />
		<?php endwhile; endif; ?>
		</div>
		<div class="col-md-6">
			<h2><?php the_title(); ?></h2>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>
			<div class="social-share">
				<p>Share with your friends:</p>
				<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" src="http://platform.twitter.com/widgets/tweet_button.1401325387.html#_=1402406010952&amp;count=horizontal&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fwww.klevermedia.co.uk%2Fassets%2Fpetcare%2Fadoption-single.html&amp;size=m&amp;text=PetCare%20Kennels%20-%20Responsive%20html%20template&amp;url=http%3A%2F%2Fwww.klevermedia.co.uk%2Fassets%2Fpetcare%2Fadoption-single.html" class="twitter-share-button twitter-tweet-button twitter-share-button twitter-count-horizontal" title="Twitter Tweet Button" data-twttr-rendered="true" style="width: 107px; height: 20px;"></iframe>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<a data-pin-href="http://www.pinterest.com/pin/create/button/" data-pin-log="button_pinit_bookmarklet" class="PIN_1402406011368_pin_it_button_20 PIN_1402406011368_pin_it_button_en_20_red PIN_1402406011368_pin_it_button_inline_20 PIN_1402406011368_pin_it_none_20" data-pin-config="none"><span class="PIN_1402406011368_hidden" id="PIN_1402406011368_pin_count_0"><i></i></span></a>
				<!-- Please call pinit.js only once per page -->
				
			</div>
		</div>
	</div>
</div>