<div class="staff" data-stellar-background-ratio=".3">
	<div class="container">
		<div class="row">
			<?php $args = array( 'post_type' => 'Team', 'orderby' => 'ID',
	'order' => 'ASC' );
				  $loop = new WP_Query( $args );
				  while ( $loop->have_posts() ) : $loop->the_post();
			?>
				<div class="col-md-3 member">
					<div>
						<span>
							<?php if ($petcare_option['Twitter-link']){ ?>
								<a href="<?php echo $petcare_option['Twitter-link']; ?>" class="twitter"></a>
							<?php } ?>
							<?php if ($petcare_option['Facebook-link']){ ?>
								<a href="<?php echo $petcare_option['Facebook-link']; ?>" class="facebook"></a>
							<?php } ?>
							<?php the_post_thumbnail(); ?>
						</span>
						<h4><?php the_title(); ?></h4>
						<?php the_content(); ?>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>