<!-- Rehome -->
<div class="rehome">
	<div class="container">
		<div class="row">
			<div class="col-md-12 centered">
				<p>
					<a href="<?php echo get_permalink($petcare_option['Featured-link']); ?>" title="Dougie" class="roundal wow bounceInDown">
						<img src="<?php echo $petcare_option['Featured-pet']['url']; ?>" alt="<?php echo $petcare_option['Featured-title']; ?>" />
					</a>
				</p>
				<h4 class="wow bounceInDown"><?php echo $petcare_option['Featured-title']; ?></h4>
				<p class="wow bounceInDown"><?php echo $petcare_option['Featured-content']; ?></p>
				<form method="get" action="<?php echo get_permalink($petcare_option['Featured-link']); ?>" class="wow bounceInDown"">	
					<button type="submit" class="btn btn-default btn-green"><?php echo $petcare_option['Button-text']; ?></button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Rehome end -->