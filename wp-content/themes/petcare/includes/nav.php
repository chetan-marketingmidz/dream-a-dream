<!-- Navigation -->
<div class="navbar navbar-default navbar-fixed-top affix-top affix" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<h1><a class="navbar-brand" href="<?php bloginfo('url'); ?>">
				<?php if($petcare_option['Logo']['url']){ ?>
					<img src="<?php echo $petcare_option['Logo']['url']; ?>" alt="<?php bloginfo('name'); ?>" />
				<?php } else { ?>
					<img src="<?php echo $petcare_option['Content-logo']['url']; ?>" alt="<?php bloginfo('name'); ?>" />
				<?php } ?>
			</a></h1>
		</div>	
		<div class="navbar-collapse collapse">
			<?php
	            wp_nav_menu( array(
	                'menu'              => 'primary',
	                'theme_location'    => 'primary',
	                'depth'             => 2,
	                'container'         => 'div',
	                'container_class'   => 'collapse navbar-collapse',
	                'container_id'      => 'bs-example-navbar-collapse-1',
	                'menu_class'        => 'nav navbar-nav',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                'walker'            => new wp_bootstrap_navwalker())
	            );
	        ?>
		</div>
	</div>
</div>
<!-- Navigation end -->
