<!-- Testimonials -->
<div class="testimonials" data-stellar-background-ratio="0.6">
	<div class="container">
		<div class="row">
			<div class="col-md-12 centered">
				<!-- Slider -->
				<div id="home_testimonial" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<?php $post_counter = "-1"; ?>
						<?php
				
							  $args = array( 'post_type' => 'Testimonials' );
							  $loop = new WP_Query( $args );
							  while ( $loop->have_posts() ) : $loop->the_post();
						?>
						<?php $post_counter++; ?>
							<li data-target="#home_testimonial" data-slide-to="<?php echo $post_counter; ?>"></li>
						<?php endwhile ?>
					</ol>
					
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<?php 
				
							  $args = array( 'post_type' => 'Testimonials' );
							  $loop = new WP_Query( $args );
							  while ( $loop->have_posts() ) : $loop->the_post();
						?>
						
						<?php endwhile; ?>
					</div>
				</div>
				<!-- Slider end -->

			</div>
		</div>
	</div>
</div>
<!-- Testimonials end -->