<!-- Prices -->
<div class="prices" data-stellar-background-ratio=".3">
	<div class="container">
		<div class="row">
			<?php $args = array( 'post_type' => 'Price', 'orderby' => 'ID',
	'order' => 'ASC' );
				  $loop = new WP_Query( $args );
				  while ( $loop->have_posts() ) : $loop->the_post();
			?>
			<div class="col-md-3 costs">
				<div>
					<?php the_post_thumbnail(); ?>
					<h4><?php the_title(); ?></h4>
					<p class="per-night"><?php echo $petcare_option['Post-Price']; ?></p>
					<?php the_content(); ?>
					<form method="get" action="<?php echo get_permalink($petcare_option['opt-select-pages']); ?>">
						<button type="submit" class="btn btn-default btn-green"><?php echo $petcare_option['button-text']; ?></button>
					</form>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<!-- Prices end -->