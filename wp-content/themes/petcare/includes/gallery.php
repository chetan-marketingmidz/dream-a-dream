<!-- Carousel -->
<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3 class="border"><span><?php echo $petcare_option['Gallery-title'] ?></span></h3>
			<?php echo $petcare_option['Gallery-content'] ?>
		</div>
	</div>
</div>
<div id="c-carousel">
	<div id="wrapper">
		<div id="carousel">
			<?php $args = array( 'post_type' => 'Gallery' );
				  $loop = new WP_Query( $args );
				  while ( $loop->have_posts() ) : $loop->the_post();
			?>
				<div class="wow flipInX">
					<a href="<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' ); echo $src[0]; ?>" title="<?php the_title(); ?>" data-hover="<?php the_title(); ?>" data-toggle="lightbox" class="lightbox">
						<?php the_post_thumbnail('Gallery'); ?>
					</a>
				</div>
			<?php endwhile; ?>
		</div>
		<div id="pager" class="pager"></div>
	</div>
</div>
<!-- Carousel end -->