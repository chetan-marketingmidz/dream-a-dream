<!-- Slider -->
<div id="home_carousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<?php $post_counter = "-1"; ?>
		<?php $args = array( 'post_type' => 'Hero' );
			  $loop = new WP_Query( $args );
			  while ( $loop->have_posts() ) : $loop->the_post();
		?>
		<?php $post_counter++; ?>
			<li data-target="#home_carousel" data-slide-to="<?php echo $post_counter; ?>"></li>
		<?php endwhile; ?>
	</ol>
	
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<?php $args = array( 'post_type' => 'Hero' );
			  $loop = new WP_Query( $args );
			  while ( $loop->have_posts() ) : $loop->the_post();
		?>
			<div class="item">
				<?php the_post_thumbnail(); ?>
				<div class="carousel-caption">
					<h2><?php the_title(); ?></h2>
				    <p><?php the_content(); ?></p>
				    <?php if($petcare_option['opt-select-pages']){ ?>
					    <form method="get" action="<?php echo get_permalink($petcare_option['opt-select-pages']); ?>">
					    	<button type="submit" class="btn btn-lg btn-default">
					    		<?php if($petcare_option['button-text']){
					    			echo $petcare_option['button-text'];
					    		} else {
					    			echo $petcare_option['Button-text'];
					    		} ?>
					    	</button>
					    </form>
					<?php } ?>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
	
	<!-- Controls -->
	<a class="left carousel-control" href="#home_carousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#home_carousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>
<!-- Slider end -->