<!-- Services -->
<div class="container">
	<div class="row">
		<?php if ($petcare_option['Service-listing'] == "1"){ ?>
			<!-- Display 3 sevices -->
			<?php
			  $args = array(
			  	'post_type' => 'Service',
			  	'post_status' => 'publish',
			    'posts_per_page' => 3
			  );
			  $loop = new WP_Query( $args );
			  while ( $loop->have_posts() ) : $loop->the_post();
			?>
				<div class="col-md-4 col3">
					<a href="<?php if($petcare_option['ext-service-link']){ echo $petcare_option['ext-service-link']; } else {  the_permalink(); } ?>" class="roundal wow bounceInUp"><?php the_post_thumbnail(); ?></a>
					<h3><?php the_title(); ?></h3>
					<p><?php echo substr(get_the_excerpt(), 0,150); ?>&hellip;</p>
					<form method="get" action="<?php if($petcare_option['ext-service-link']){ echo $petcare_option['ext-service-link']; } else {  the_permalink(); } ?>">
						<button type="submit" class="btn btn-default btn-green"><?php echo $petcare_option['Button-text']; ?></button>
					</form>
				</div>
			<?php endwhile; ?>
			
		<?php } elseif ($petcare_option['Service-listing'] == "2"){ ?>
			
			<!-- Display 6 sevices -->
			<?php
			  $args = array(
			  	'post_type' => 'Service',
			  	'post_status' => 'publish',
			    'posts_per_page' => 6
			  );
			  $loop = new WP_Query( $args );
			  while ( $loop->have_posts() ) : $loop->the_post();
			?>
				<div class="col-md-4 col3">
					<a href="<?php if($petcare_option['ext-service-link']){ echo $petcare_option['ext-service-link']; } else {  the_permalink(); } ?>" title="<?php the_title(); ?>" title="<?php the_title(); ?>" class="roundal wow rollIn"><?php the_post_thumbnail(); ?></a>
					<h3><?php the_title(); ?></h3>
					<p><?php echo substr(get_the_excerpt(), 0,150); ?>&hellip;</p>
					<form method="get" action="<?php if($petcare_option['ext-service-link']){ echo $petcare_option['ext-service-link']; } else {  the_permalink(); } ?>">
						<button type="submit" class="btn btn-default btn-green"><?php echo $petcare_option['Button-text']; ?></button>
					</form>
				</div>
			<?php endwhile; ?>
			
		<?php } elseif ($petcare_option['Service-listing'] == "3"){ ?>
			
			<!-- Display 9 sevices -->
			<?php
			  $args = array(
			  	'post_type' => 'Service',
			  	'post_status' => 'publish',
			    'posts_per_page' => 9
			  );
			  $loop = new WP_Query( $args );
			  while ( $loop->have_posts() ) : $loop->the_post();
			?>
				<div class="col-md-4 col3">
					<a href="<?php if($petcare_option['ext-service-link']){ echo $petcare_option['ext-service-link']; } else {  the_permalink(); } ?>" title="<?php the_title(); ?>" title="<?php the_title(); ?>" class="roundal wow bounceInUp"><?php the_post_thumbnail(); ?></a>
					<h3><?php the_title(); ?></h3>
					<p><?php echo substr(get_the_excerpt(), 0,150); ?>&hellip;</p>
					<form method="get" action="<?php if($petcare_option['ext-service-link']){ echo $petcare_option['ext-service-link']; } else {  the_permalink(); } ?>">
						<button type="submit" class="btn btn-default btn-green"><?php echo $petcare_option['Button-text']; ?></button>
					</form>
				</div>
			<?php endwhile; ?>
			
		<?php } ?>
	</div>
</div>
<!-- Services end -->