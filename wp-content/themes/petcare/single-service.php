<?php
/*

Service post
Description: Post template with a content container and right sidebar.

*/
get_header(); ?>

<?php include(TEMPLATEPATH . '/includes/inner-nav.php'); ?>


<?php include(TEMPLATEPATH . '/includes/services-content.php'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3><span><?php echo $petcare_option['Services-title']; ?></span></h3>
			<?php echo $petcare_option['Services-content']; ?>
		</div>
	</div>
</div>
<?php include(TEMPLATEPATH . '/includes/services.php'); ?>
<?php
if ($petcare_option['custompost-quote'] == "1"){
	include(TEMPLATEPATH . '/includes/quote.php');
}
?>
<?php get_footer(); ?>