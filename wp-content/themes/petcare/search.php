<?php
/*

Search Results Template

*/
get_header();

include(TEMPLATEPATH . '/includes/inner-nav.php'); ?>
<!-- Blog -->
<div class="container blog-holder">
	<div class="row">
		<div class="col-md-9 posts">
			<div class="row">
				<?php if (have_posts()) : while (have_posts()) : the_post();?>
					<!-- Single post -->
						<div class="col-md-12">
							<div <?php post_class(); ?>>
								<h2><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><span><?php the_title();?></span></a></h2>
								<p class="post-info">
									<?php the_date("j"); ?> <?php the_time("M"); ?> <span>|</span>
									Posted by <?php the_author(); ?> <span>|</span>
									<a href="<?php comments_link(); ?>" title="<?php comments_number( 'no comments', '1 comment', '% comments' ); ?>"><?php comments_number( 'no comments', '1 comment', '% comments' ); ?></a>  <span>|</span>
									<?php the_tags('', ', ', '<br />'); ?> 
									</p>
								<?php if(has_post_thumbnail()) :?>
									<p class="post-img-wrap">
										<a href="<?php the_permalink(); ?>" title="<?php the_title();?>" class="post-image wow bounceIn" data-wow-offset="100">
											<?php the_post_thumbnail(); ?>
										</a>
									</p>
								<?php endif;?>
								<?php the_excerpt(); ?>
							</div>
						</div>
					<!-- Single post end -->
				<?php endwhile; else: ?>
					<p><?php _e('Sorry, no posts matched your criteria.', 'inkd'); ?></p>
				<?php endif; ?>
			</div>
		</div>
		<div class="col-md-3 sidebar">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<!-- Blog end -->
<!-- Pagination -->
<div class="pagination-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php numeric_posts_pagination(); ?>
			</div>
		</div>
	</div>
</div>
<!-- Pagination end -->
<?php get_footer(); ?>