<?php
/*

  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
  
*/

// Due to a limitations of variables and functions, you must globally define
// variables inside functions.
global $petcare_settings; // This is your opt_name.
print_r($petcare_settings);

if (!class_exists('Redux_Framework_sample_config')) {

    class Redux_Framework_sample_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );
            
            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 2);
            
            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
            
            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );
            
            // Dynamically add a section. Can be also used to modify sections/fields
            //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field	set with compiler=>true is changed.

         * */
        function compiler_action($options, $css) {
            //echo '<h1>The compiler hook has run!';
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

            /*
              // Demo of how to use the dynamic CSS and write your own static CSS file
              $filename = dirname(__FILE__) . '/style' . '.css';
              global $wp_filesystem;
              if( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
              WP_Filesystem();
              }

              if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
              }
             */
        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => __('Section via hook', 'redux-framework-demo'),
                'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode('.', $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'redux-framework-demo'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                <?php endif; ?>

                <h4><?php echo $this->theme->display('Name'); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(__('By %s', 'redux-framework-demo'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(__('Version %s', 'redux-framework-demo'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . __('Tags', 'redux-framework-demo') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
            <?php
            if ($this->theme->parent()) {
                printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'redux-framework-demo'), $this->theme->parent()->display('Name'));
            }
            ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $sampleHTML = '';
            if (file_exists(dirname(__FILE__) . '/info-html.html')) {
                /** @global WP_Filesystem_Direct $wp_filesystem  */
                global $wp_filesystem;
                if (empty($wp_filesystem)) {
                    require_once(ABSPATH . '/wp-admin/includes/file.php');
                    WP_Filesystem();
                }
                $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
            }

            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'title'     => __('General options', 'redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'icon'      => 'el-icon-cog',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(
                    array(
                        'id'        => 'Logo',
                        'type'      => 'media',
                        'title'     => __('Upload Home page logo', 'redux-framework-demo'),
                        'compiler'  => 'true',
                        'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('Please upload your themes logo', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                    ),
                    array(
                        'id'        => 'Content-logo',
                        'type'      => 'media',
                        'title'     => __('Upload Content page logo', 'redux-framework-demo'),
                        'compiler'  => 'true',
                        'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('Please upload your themes logo', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                    ),
                    array(
                        'id'        => 'Button-text',
                        'type'      => 'text',
                        'title'     => __('Global button text', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Please enter the text you would like to display on all buttons', 'redux-framework-demo'),
                        'default'   => 'Learn more',
                    ),
                    array(
                        'id'        => 'TestimonialBG',
                        'type'      => 'media',
                        'title'     => __('Upload testimonials background', 'redux-framework-demo'),
                        'compiler'  => 'true',
                        'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                    ),
                    array(
                        'id'        => 'Favicon',
                        'type'      => 'media',
                        'title'     => __('Upload favicon', 'redux-framework-demo'),
                        'compiler'  => 'true',
                        'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                    ),
                ),
            );
            /**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'icon'      => 'el-icon-pencil',
                'title'     => __('Styling &amp; Layout', 'redux-framework-demo'),
                'fields'    => array(
                	array(
                        'id'        => 'Accent-colour',
                        'type'      => 'color',
                        'output'    => array('.site-title'),
                        'title'     => __('Accent Color', 'redux-framework-demo'),
                        'subtitle'  => __('Pick a color for the theme.', 'redux-framework-demo'),
                        'default'   => '#65c178',
                        'validate'  => 'color',
                    ),
                    array(
                        'id'        => 'custompost-quote',
                        'type'      => 'select',
                        'title'     => __('Show quote on cusomt posts', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => 'Yes', 
                            '2' => 'No', 
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'opt-homepage-layout',
                        'type'      => 'sorter',
                        'title'     => 'Layout Manager ',
                        'subtitle'  => 'Organise how you want the layout on the Home page.',
                        'compiler'  => 'true',
                        'options'   => array(
                            'enabled'   => array(
                            	"placebo" => "placebo", //REQUIRED!
                                'nav'			=> 'Nav',
                                'hero'			=> 'Hero',
                                'services'		=> 'Services',
                                'gallery'		=> 'Gallery',
                                'featured'		=> 'Featured pet',
                                'testimonials'	=> 'Testimonials',
                                'adoption'		=> 'Adoption',
                                'quote'			=> 'Quote'
                            ),
                            'disabled'  => array(
                            	"placebo" => "placebo", //REQUIRED!
                            ),
                            'backup'    => array(
                            	"placebo" => "placebo", //REQUIRED!
                            ),
                        ),
                    ),
                    array(
                        'id'        => 'ace-editor-css',
                        'type'      => 'ace_editor',
                        'title'     => __('CSS Code', 'redux-framework-demo'),
                        'subtitle'  => __('Paste your CSS code here.', 'redux-framework-demo'),
                        'mode'      => 'css',
                        'theme'     => 'monokai',
                        'desc'      => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
                        'default'   => ""
                    ),
                )
            );
            
            /**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'icon'      => 'el-icon-guidedog',
                'title'     => __('Service options', 'redux-framework-demo'),
                'fields'    => array(
                    array(
                        'id'        => 'Service-listing',
                        'type'      => 'select',
                        'title'     => __('Display services', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Please select how many services you would like to be displayed on the Home/Services page.', 'redux-framework-demo'),
                        
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => '3', 
                            '2' => '6', 
                            '3' => '9'
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'Services-title',
                        'type'      => 'text',
                        'title'     => __('Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('This will be displayed on the Services pages.', 'redux-framework-demo'),
                        'default'   => ''
                    ),
                    array(
						'id'=>'Services-content',
						'type' => 'editor',
						'title' => __('Content', 'redux-framework'), 
						//'subtitle' => __('', 'redux-framework'),
						//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
					),
                )
            );
            
            /**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'icon'      => 'el-icon-photo',
                'title'     => __('Gallery options', 'redux-framework-demo'),
                'fields'    => array(
                    array(
                        'id'        => 'Gallery-title',
                        'type'      => 'text',
                        'title'     => __('Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('This will be displayed on the Home/Content pages.', 'redux-framework-demo'),
                        'default'   => ''
                    ),
                    array(
						'id'=>'Gallery-content',
						'type' => 'editor',
						'title' => __('Content', 'redux-framework'), 
						//'subtitle' => __('', 'redux-framework'),
						//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
					),
                )
            );
            

            /**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'icon'      => 'el-icon-map-marker',
                'title'     => __('Contact options', 'redux-framework-demo'),
                'heading'   => __('Contact options', 'Redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'fields'    => array(
                	array(
                        'id'        => 'EmailAddress',
                        'type'      => 'text',
                        'title'     => __('Email address', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'validate'  => 'email',
                        'msg'       => 'Please enter a correct email format.',
                        'default'   => '',
//                        'text_hint' => array(
//                            'title'     => 'Valid Email Required!',
//                            'content'   => 'This field required a valid email address.'
//                        )
                    ),
                    array(
                        'id'        => 'Telephone',
                        'type'      => 'text',
                        'title'     => __('Telephone', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'default'   => ''
                    ),
                    array(
                        'id'        => 'Address',
                        'type'      => 'textarea',
                        'title'     => __('Address', 'redux-framework-demo'),
                        'subtitle'  => __('All HTML will be stripped', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'validate'  => 'no_html',
                        'default'   => 'No HTML is allowed in here.'
                    ),
                    array(
                        'id'        => 'Long',
                        'type'      => 'text',
                        'title'     => __('Long', 'redux-framework-demo'),
                        'subtitle'  => __('This may <a href="http://itouchmap.com/latlong.html">help</a>.', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'default'   => '51.733472'
                    ),
                    array(
                        'id'        => 'Lat',
                        'type'      => 'text',
                        'title'     => __('Lat', 'redux-framework-demo'),
                        'subtitle'  => __('This may <a href="http://itouchmap.com/latlong.html">help</a>.', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'default'   => '0.678792'
                    ),
                    array(
                        'id'        => 'Pin',
                        'type'      => 'media',
                        'title'     => __('Upload pin', 'redux-framework-demo'),
                        'compiler'  => 'true',
                        'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('Please upload your map pin.', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                    ),
                    array(
                        'id'        => 'PinWidth',
                        'type'      => 'text',
                        'title'     => __('Pin width', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Please enter numerics only (no "px" required).', 'redux-framework-demo'),
                        'default'   => '273'
                    ),
                    array(
                        'id'        => 'PinHeight',
                        'type'      => 'text',
                        'title'     => __('Pin height', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Please enter numerics only (no "px" required).', 'redux-framework-demo'),
                        'default'   => '85'
                    ),
                )
            );
            
            /**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'icon'      => 'el-icon-heart',
                'title'     => __('Adoption options', 'redux-framework-demo'),
                'heading'   => __('Adoption options', 'Redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'fields'    => array(
                	array(
                        'id'        => 'Adoption-listing',
                        'type'      => 'select',
                        'title'     => __('Display adoption blocks', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Please select how many adoption blocks you would like to be displayed on the Home page.', 'redux-framework-demo'),
                        
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => '3', 
                            '2' => '6', 
                            '3' => '9'
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'Adoption-title',
                        'type'      => 'text',
                        'title'     => __('Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('This will be displayed on the Home page and single adoption page.', 'redux-framework-demo'),
                        'default'   => ''
                    ),
                    array(
						'id'=>'Adoption-content',
						'type' => 'editor',
						'title' => __('Content', 'redux-framework'), 
						//'subtitle' => __('', 'redux-framework'),
						//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
					),
                )
            );
			
			/**********************************************Achive Page Title**************************************************/
			
			
			/**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'title'     => __('Archive Page Titles &amp; Contents', 'redux-framework-demo'),
                'heading'   => __('Archive Page Titles &amp; Contents', 'Redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'fields'    => array(
                	array(
                        'id'        => 'archive-puppy-title',
                        'type'      => 'text',
                        'title'     => __('Puppies Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Show on archive page.', 'redux-framework-demo'),
                        'default'   => ''
                    ), 
					array(
						'id'=>'puppy-content',
						'type' => 'editor',
						'title' => __('Content', 'redux-framework'), 
						//'subtitle' => __('', 'redux-framework'),
						//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
					),
					array(
                        'id'        => 'archive-showring-title',
                        'type'      => 'text',
                        'title'     => __('Showring Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Show on archive page.', 'redux-framework-demo'),
                        'default'   => ''
                    ), 
					array(
						'id'=>'showring-content',
						'type' => 'editor',
						'title' => __('Content', 'redux-framework'), 
						//'subtitle' => __('', 'redux-framework'),
						//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
					),
					array(
                        'id'        => 'archive-adoption-title',
                        'type'      => 'text',
                        'title'     => __('Adoption Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Show on archive page.', 'redux-framework-demo'),
                        'default'   => ''
                    ), 
					array(
						'id'=>'adoption-content',
						'type' => 'editor',
						'title' => __('Content', 'redux-framework'), 
						//'subtitle' => __('', 'redux-framework'),
						//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
					),
                )
            );
			
			
			/****************************************************Achive Page Title**********************************************/
			
			
            /**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'icon'      => 'el-icon-guidedog',
                'title'     => __('Puppies options', 'redux-framework-demo'),
                'heading'   => __('Puppies options', 'Redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'fields'    => array(
                	array(
                        'id'        => 'Puppies-listing',
                        'type'      => 'select',
                        'title'     => __('Display puppies blocks', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Please select how many puppies blocks you would like to be displayed on the Home page.', 'redux-framework-demo'),
                        
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => '3', 
                            '2' => '6', 
                            '3' => '9'
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'Puppies-title',
                        'type'      => 'text',
                        'title'     => __('Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('This will be displayed on the Home page and single puppies page.', 'redux-framework-demo'),
                        'default'   => ''
                    ),
                    array(
						'id'=>'Puppies-content',
						'type' => 'editor',
						'title' => __('Content', 'redux-framework'), 
						//'subtitle' => __('', 'redux-framework'),
						//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
					),
                )
            );
			/**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'icon'      => 'el-icon-guidedog',
                'title'     => __('Showring options', 'redux-framework-demo'),
                'heading'   => __('Showring options', 'Redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'fields'    => array(
                	array(
                        'id'        => 'Showring-listing',
                        'type'      => 'select',
                        'title'     => __('Display showring blocks', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Please select how many showring blocks you would like to be displayed on the Home page.', 'redux-framework-demo'),
                        
                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => '3', 
                            '2' => '6', 
                            '3' => '9'
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'Showring-title',
                        'type'      => 'text',
                        'title'     => __('Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('This will be displayed on the Home page and single showring page.', 'redux-framework-demo'),
                        'default'   => ''
                    ),
                    array(
						'id'=>'Showring-content',
						'type' => 'editor',
						'title' => __('Content', 'redux-framework'), 
						//'subtitle' => __('', 'redux-framework'),
						//'default' => 'Powered by [wp-url]. Built on the [theme-url].',
					),
                )
            );
            /**
             *  Note here I used a 'heading' in the sections array construct
             *  This allows you to use a different title on your options page
             * instead of reusing the 'title' value.  This can be done on any
             * section - kp
             */
            $this->sections[] = array(
                'icon'      => 'el-icon-star',
                'title'     => __('Featured pet', 'redux-framework-demo'),
                'heading'   => __('Featured pet', 'Redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'fields'    => array(
                	array(
                        'id'        => 'Featured-pet',
                        'type'      => 'media',
                        'url'		=> true,
                        'title'     => __('Upload thumbnail of featured pet', 'redux-framework-demo'),
                        'compiler'  => 'true',
                        'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'      => __('Please upload an image of 127 x 127px.', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                    ),
                    array(
                        'id'        => 'Featured-title',
                        'type'      => 'text',
                        'title'     => __('Title', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'default'   => ''
                    ),
                    array(
                        'id'        => 'Featured-content',
                        'type'      => 'textarea',
                        'title'     => __('Custom HTML', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'validate'  => 'html',
                    ),
                    array(
                        'id'        => 'Featured-link',
                        'type'      => 'select',
                        'data'      => 'pages',
                        'title'     => __('Link to page', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('Link to your single pet page.', 'redux-framework-demo'),
                    ),
                    array(
                        'id'        => 'Pet-name',
                        'type'      => 'text',
                        'title'     => __('Pet name', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'default'   => ''
                    ),
                    array(
		                'id'        => 'Feat-slides',
		                'type'      => 'slides',
		                'title'     => __('Featured pet gallery', 'redux-framework-demo'),
		                'subtitle'  => __('Upload images of the featured pet.', 'redux-framework-demo'),
		                'desc'      => __('', 'redux-framework-demo'),
		                'placeholder'   => array(
		                    'title'         => __('This is a title', 'redux-framework-demo'),
		                    'description'   => __('Description Here', 'redux-framework-demo'),
		                    'url'           => __('Give us a link!', 'redux-framework-demo'),
		                ),
		            ),
                )
            );
            
            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'title'     => __('Quote block', 'redux-framework-demo'),
                'desc'      => __('', 'redux-framework-demo'),
                'icon'      => 'el-icon-home',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'Quote-para',
                        'type'      => 'text',
                        'title'     => __('Paragraph', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'default'   => 'PetCare is a totally awesome unique & responsive HTML template',
                    ),
                    
                    array(
                        'id'        => 'Quote-subpara',
                        'type'      => 'text',
                        'title'     => __('Sub Paragraph', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'default'   => 'Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.',
                    ),
                    
                    array(
                        'id'        => 'Quote-buttontxt',
                        'type'      => 'text',
                        'title'     => __('Button text', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'default'   => 'Purchase theme',
                    ),
                    
                    array(
                        'id'        => 'Quote-link',
                        'type'      => 'select',
                        'data'      => 'pages',
                        'title'     => __('Link to page', 'redux-framework-demo'),
                        'subtitle'  => __('', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                    ),
                    
                ),
            );
            
            $this->sections[] = array(
                'icon'      => 'el-icon-website',
                'title'     => __('Footer options', 'redux-framework-demo'),
                'fields'    => array(
                    array(
                        'id'        => 'Analytics-code',
                        'type'      => 'text',
                        'title'     => __('Tracking Code', 'redux-framework-demo'),
                        'subtitle'  => __('Paste your Google Analytics tracking ID.', 'redux-framework-demo'),
                        'validate'  => 'js',
                        'desc'      => '',
                    ),
                    array(
                        'id'        => 'Twitter-url',
                        'type'      => 'text',
                        'title'     => __('Twitter URL', 'redux-framework-demo'),
                        'subtitle'  => __('This must be a URL.', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'validate'  => 'url',
                        'default'   => '',
                    ),
                    array(
                        'id'        => 'Facebook-url',
                        'type'      => 'text',
                        'title'     => __('Facebook URL', 'redux-framework-demo'),
                        'subtitle'  => __('This must be a URL.', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'validate'  => 'url',
                        'default'   => '',
                    ),
                    array(
                        'id'        => 'LinkedIn-url',
                        'type'      => 'text',
                        'title'     => __('LinkedIn URL', 'redux-framework-demo'),
                        'subtitle'  => __('This must be a URL.', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'validate'  => 'url',
                        'default'   => '',
                    ),
                    array(
                        'id'        => 'Pinterest-url',
                        'type'      => 'text',
                        'title'     => __('Pinterest URL', 'redux-framework-demo'),
                        'subtitle'  => __('This must be a URL.', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'validate'  => 'url',
                        'default'   => '',
                    ),
                    array(
                        'id'        => 'GooglePlus-url',
                        'type'      => 'text',
                        'title'     => __('Google + URL', 'redux-framework-demo'),
                        'subtitle'  => __('This must be a URL.', 'redux-framework-demo'),
                        'desc'      => __('', 'redux-framework-demo'),
                        'validate'  => 'url',
                        'default'   => '',
                    ),
                    array(
                        'id'        => 'Footer-text',
                        'type'      => 'editor',
                        'title'     => __('Footer Text', 'redux-framework-demo'),
                        'subtitle'  => __('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'redux-framework-demo'),
                        'default'   => '<p>&copy; <strong>[site-title]</strong> [current-year] All rights reserved.</p>',
                    ),
                )
            );
            
        
            $theme_info  = '<div class="redux-framework-section-desc">';
            $theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . __('<strong>Theme URL:</strong> ', 'redux-framework-demo') . '<a href="' . $this->theme->get('ThemeURI') . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-author">' . __('<strong>Author:</strong> ', 'redux-framework-demo') . $this->theme->get('Author') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-version">' . __('<strong>Version:</strong> ', 'redux-framework-demo') . $this->theme->get('Version') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
            $tabs = $this->theme->get('Tags');
            if (!empty($tabs)) {
                $theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . __('<strong>Tags:</strong> ', 'redux-framework-demo') . implode(', ', $tabs) . '</p>';
            }
            $theme_info .= '</div>';

            if (file_exists(dirname(__FILE__) . '/../README.md')) {
                $this->sections['theme_docs'] = array(
                    'icon'      => 'el-icon-list-alt',
                    'title'     => __('Documentation', 'redux-framework-demo'),
                    'fields'    => array(
                        array(
                            'id'        => '17',
                            'type'      => 'raw',
                            'markdown'  => true,
                            'content'   => file_get_contents(dirname(__FILE__) . '/../README.md')
                        ),
                    ),
                );
            }
            
            $this->sections[] = array(
                'title'     => __('Import / Export', 'redux-framework-demo'),
                'desc'      => __('Import and Export your Redux Framework settings from file, text or URL.', 'redux-framework-demo'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
                    ),
                ),
            );                     
                    
            $this->sections[] = array(
                'type' => 'divide',
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-info-sign',
                'title'     => __('Theme Information', 'redux-framework-demo'),
                'desc'      => __('<p class="description">This is the Description. Again HTML is allowed</p>', 'redux-framework-demo'),
                'fields'    => array(
                    array(
                        'id'        => 'opt-raw-info',
                        'type'      => 'raw',
                        'content'   => $item_info,
                    )
                ),
            );

            if (file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
                $tabs['docs'] = array(
                    'icon'      => 'el-icon-book',
                    'title'     => __('Documentation', 'redux-framework-demo'),
                    'content'   => nl2br(file_get_contents(trailingslashit(dirname(__FILE__)) . 'README.html'))
                );
            }
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => __('Theme Information 1', 'redux-framework-demo'),
                'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo')
            );

            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-2',
                'title'     => __('Theme Information 2', 'redux-framework-demo'),
                'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo');
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'petcare_option',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => __('Theme Options', 'redux-framework-demo'),
                'page_title'        => __('Theme Options', 'redux-framework-demo'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => '', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                
                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );


            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            $this->args['share_icons'][] = array(
                'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
                'title' => 'Visit us on GitHub',
                'icon'  => 'el-icon-github'
                //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
            );
            $this->args['share_icons'][] = array(
                'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                'title' => 'Like us on Facebook',
                'icon'  => 'el-icon-facebook'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'http://twitter.com/reduxframework',
                'title' => 'Follow us on Twitter',
                'icon'  => 'el-icon-twitter'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'http://www.linkedin.com/company/redux-framework',
                'title' => 'Find us on LinkedIn',
                'icon'  => 'el-icon-linkedin'
            );

            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }
                $this->args['intro_text'] = sprintf(__('', 'redux-framework-demo'), $v);
            } else {
                $this->args['intro_text'] = __('', 'redux-framework-demo');
            }

            
        }

    }
    
    global $reduxConfig;
    $reduxConfig = new Redux_Framework_sample_config();
}

/**
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;

/**
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';

        /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            $field['msg'] = 'your custom error message';
          }
         */

        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;