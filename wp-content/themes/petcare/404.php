<?php
/*

The template for displaying 404 pages (Not Found).

*/
get_header(); ?>

<?php include(TEMPLATEPATH . '/includes/inner-nav.php'); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12 centered page-not-found">
			<h3><span>404</span></h3>
			<p>Well, this is awkward. We can't actually find this page.</p>
		</div>
	</div>
</div>
<?php include(TEMPLATEPATH . '/includes/testimonials.php'); ?>
<?php include(TEMPLATEPATH . '/includes/services.php'); ?>
<?php include(TEMPLATEPATH . '/includes/quote.php'); ?>

<?php get_footer(); ?>