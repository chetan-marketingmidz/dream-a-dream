<?php
/*

Adoption post
Description: Post template with a content container and right sidebar.

*/
get_header(); ?>

<?php require_once(TEMPLATEPATH . '/includes/inner-nav.php'); ?>


<?php require_once(__DIR__ . '/includes/showring-content.php'); ?>
<?php require_once(__DIR__ . '/includes/showring.php'); ?>
<?php
if ($petcare_option['custompost-quote'] == "1"){
	require_once(TEMPLATEPATH . '/includes/quote.php');
}
?>

<?php get_footer(); ?>