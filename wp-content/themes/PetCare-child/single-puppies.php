<?php
/*

Adoption post
Description: Post template with a content container and right sidebar.

*/
get_header();
global $petcare_option;

require_once(TEMPLATEPATH . '/includes/inner-nav.php');
require_once(__DIR__. '/includes/puppies-content.php');
require_once(__DIR__ . '/includes/puppies.php');

if ($petcare_option['custompost-quote'] == "1"){
	require(TEMPLATEPATH . '/includes/quote.php');
}
?>

<?php get_footer(); ?>