<?php
include "../../../wp-load.php";
$animalid = isset($_POST['animalid'])&&!empty($_POST['animalid']) ? $_POST['animalid'] : '';
$post_type = isset($_POST['post_type'])&&!empty($_POST['post_type']) ? $_POST['post_type'] : '';
global $pedigree;
if($animalid!=''){
	$result_array=array();
	$mydb = new wpdb('root','','dogspot','localhost');
	
	$title='';
	$name_row = $mydb->get_row("select name,shortname,personid from name where animalid='".$animalid."'");
	
	$personid=$name_row->personid;
	$result_array['name']=$name_row->name;
	$result_array['shortname']=$name_row->shortname;
		
	$animal_row = $mydb->get_row("select * from animal where animalid='".$animalid."'");	

	$result_array['dob']=$animal_row->dob;
	$result_array['color']=$animal_row->color;
	$result_array['sex'] = $animal_row->sex;
	
	$description_row = $mydb->get_row("select * from description where animalid='".$animalid."'");
	$result_array['description']  = $description_row->description;
	
	$price_row = $mydb->get_row("select * from asking_price where animalid='".$animalid."'");
	$result_array['price']  = $price_row->price;
	
	$photo_sql = "SELECT * FROM animal_photo JOIN photo ON animal_photo.photoid = photo.photoid WHERE animalid='".$animalid."'";
	$photo_row = $mydb->get_row($photo_sql);
	$result_array['photo_url']  = $photo_row->file_path;
	
	if($post_type=='showring'){
		$level = 1;
		get_pedigree_3gen($animalid, $level);
		$result_array['pedigree']=$pedigree;
	}
	echo json_encode($result_array);
}
?>