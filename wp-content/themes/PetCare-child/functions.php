<?php

global $petcare_option; 

function patcare_scripts() {
	// Load our main stylesheet.
	wp_enqueue_style( 'petcare-style', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'patcare_scripts' );

function get_pedigree_3gen($animal_id,$level){
	global $pedigree;
	global $wpdb;

	$mydb = new wpdb('root','','dogspot','localhost');
	
	//echo "select momid,dadid from child where animalid='".$animal_id."'";
	$pedigree_info = $mydb->get_row("select momid,dadid from child where animalid='".$animal_id."'");
	
	$puppy_mom = $wpdb->get_row("SELECT `post_id` FROM `wp_postmeta` WHERE `meta_key` = 'animalid' and `meta_value`='".$pedigree_info->momid."'");
	$puppy_dad = $wpdb->get_row("SELECT `post_id` FROM `wp_postmeta` WHERE `meta_key` = 'animalid' and `meta_value`='".$pedigree_info->dadid."'");

	if(count($puppy_mom)<1){
		$pmom='';
	}
	else{
		$pmom=$puppy_mom->post_id;
	}
	
	if(count($puppy_dad)<1){
		$pdad='';
	}
	else{
		$pdad=$puppy_dad->post_id;
	}
	
	$pedigree[]=array(
		'momid'  => $pmom,
		'dadid'  => $pdad
	);		

	if($level<2){
		$level+=1;
		get_pedigree_3gen($pedigree_info->momid,$level);
		get_pedigree_3gen($pedigree_info->dadid,$level);
	}
	else{
		return json_encode($pedigree);
	}
}
	
function check_vaccination($post_id, $vac_name, $date_of_dos){
	$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
	$vaccination_types = wp_get_post_terms( $post_id, 'vaccination_types', $args );
	foreach($vaccination_types as $vaccination_type){
		if($vaccination_type->name == $vac_name){
			echo $check_mark = '<img src="'.site_url().'/wp-content/uploads/2015/05/b_check.png"/>';
		}
	}
}

function getAge($dob, $given_date){
	$birth = strtotime($dob);
	$t = strtotime($given_date);
	$age = ($birth < 0) ? ( $t + ($birth * -1) ) : $t - $birth;
	//return floor($age/31536000);
	$totalAge = floor($age/86400);
	/*if($totalAge >= 7){
		
	}*/
	return $totalAge;
	
}
	
add_action('init', 'dream_custom_post_type_taxonomies');
function dream_custom_post_type_taxonomies() {
	
	/* Register Puppies Custom Post Type */
	register_post_type('Puppies', array(
		'labels' => array(
			'name' => 'Puppies',
			'singular_name' => 'Puppies',
			'add_new' => 'Add new Puppy',
			'edit_item' => 'Edit Puppy',
			'new_item' => 'New Puppy',
			'view_item' => 'View Puppy',
			'search_items' => 'Search Puppies',
			'not_found' => 'No Puppies found',
			'not_found_in_trash' => 'No Puppies found in Trash',
		),
		'public' => true,
		'rewrite' => array('slug' => 'puppies'),
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields', 'page-attributes'),
		'has_archive' => true,
	));
	
	
	/* Register Vaccination Custom Post Type */
	$vacc_labels = array(
		'name' => __('Vaccination Info', 'post type general name'),
		'singular_name' => __('Vaccination Info', 'post type singular name'),
		'add_new' => __('Add Vaccination Info', 'portfolio item'),
		'add_new_item' => __('Add Vaccination Info'),
		'edit_item' => __('Edit Vaccination Info'),
		'new_item' => __('New Vaccination Info'),
		'view_item' => __('View Vaccination Info'),
		'search_items' => __('Search Vaccination Info'),
		'not_found' =>__('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
	$vacc_args = array(
		'labels' => $vacc_labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title', 'thumbnail')
	);
	register_post_type( 'vaccinationinfo' , $vacc_args );
	
	/* Register Vaccination Custom Taxonomy */
	$vt_labels = array(
		'name'              => _x( 'Vaccination Types', 'taxonomy general name' ),
		'singular_name'     => _x( 'Vaccination Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Vaccination Types' ),
		'all_items'         => __( 'All Vaccination Types' ),
		'parent_item'       => __( 'Parent Vaccination Type' ),
		'parent_item_colon' => __( 'Parent Vaccination Type:' ),
		'edit_item'         => __( 'Edit Vaccination Type' ),
		'update_item'       => __( 'Update Vaccination Type' ),
		'add_new_item'      => __( 'Add New Vaccination Type' ),
		'new_item_name'     => __( 'New Vaccination Type' ),
		'menu_name'         => __( 'Vaccination Types' ),
	);
	$vt_args = array(
		'hierarchical'      => true,
		'labels'            => $vt_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'vaccination_types' ),
	);
	register_taxonomy( 'vaccination_types', array( 'vaccinationinfo' ), $vt_args );

	/* Register Show Ring Custom Post Type */
	register_post_type('Show Ring', array(		
		'labels' => array(		
			'name' => 'Show Ring',			
			'singular_name' => 'Show Ring',			
			'add_new' => 'Add new Show Ring',			
			'edit_item' => 'Edit Show Ring',			
			'new_item' => 'New Show Ring',			
			'view_item' => 'View Show Ring',			
			'search_items' => 'Search Show Ring',			
			'not_found' => 'No Show Ring found',			
			'not_found_in_trash' => 'No Show Ring found in Trash',
		),
		'has_archive' => true,		
		'public' => true,		
		'rewrite' => array('slug' => 'showring'),		
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields')	
	));
}

/**
 * Craete Meta Boxes For Custom Post Type
 *
**/
function add_custom_meta_box()
{
	add_meta_box("select_animal", "Choose Puppy", "custom_meta_box_for_puppies", "puppies", "side", "high", null);
	
	add_meta_box( 'select_puppy_in_vaccination', 'Select Dog Here', 'custom_meta_box_for_vaccination', 'vaccinationinfo', 'normal', 'high' );
	
	add_meta_box("select_animal_showring", "Show Ring", "custom_meta_box_showring", "showring", "side", "high", null);
	add_meta_box("select_animal_adoption", "Show Ring", "custom_meta_box_for_adoption", "adoption", "side", "high", null);
		
	add_meta_box("select_pedigree", "pedigree", "custom_meta_box_pedigree", "showring", "normal", "high", null);
}
add_action("add_meta_boxes", "add_custom_meta_box");


/* ADD CUSTOM META BOX For Puppies */
function custom_meta_box_for_puppies($object){
	global $post;
	
	$post_type=$post->post_type;
	$animalid=get_post_meta($post->ID,'animalId',true);
	$mydb = new wpdb('root','','dogspot','localhost');
	$rows = $mydb->get_results("select name,animalid from name");
	?>
	<form name="animalfrm" id="animalfrm" method="post">
		<select name="animalid" id="animalid">
            <option value="">Select Animal</option>
            <?php foreach ($rows as $row) : ?>
                <option value="<?php echo $row->animalid; ?>" <?php if($animalid==$row->animalid){ ?> selected="selected" <?php } ?>><?php echo $row->name ?></option>
            <?php endforeach; ?>
		</select>
        <input type="hidden" id="post_type" name="post_type" value="<?php echo $post_type ?>" />
		<input type="button" id="get_info" name="get_info" value="Get Info" />
	</form>
	<script type="text/javascript">
        jQuery(document).ready(function(e) {
            console.log("on load");
            jQuery('body').on('click','#get_info',function(){
                
                console.log("on button click")
                var url = "<?php echo get_template_directory_uri();?>-child/~get_puppy_info.php";
                var animalid = jQuery("#animalid").val();
				var post_type = jQuery("#post_type").val();
    			//console.log(animalid)
                jQuery.ajax({
                    url: url,
                    type: "POST",
					//dataType:"TEXT",
                    dataType:"JSON",
                    data: {
                        animalid:animalid,
						post_type:post_type
                    },
                    success: function(result) {
                        console.log(result);					
                        jQuery("#title").val(result.name);
                        jQuery("#acf-field-short_name").val(result.shortname);
                        jQuery("#content").val(result.description);
                        jQuery("#acf-field-sex").val(result.sex);
                        jQuery("#acf-field-color").val(result.color);
                        jQuery(".acf-date_picker").find('input').val(result.dob);
                        jQuery("#acf-field-price").val(result.price);
                        jQuery("#acf-vaccination_info").val(result.vaccination_type);
						jQuery("#acf-field-price").val(result.price);
						jQuery("#acf-field-photo_url").val(result.photo_url);
                    },
                    error: function(e) {
						console.log(e);
                    }
                });			
            });  
        });
    </script>
<?php } 

/* ADD CUSTOM META BOX For Puppies */
function custom_meta_box_for_vaccination( $post ) {
?>
    <form method="post">
        <p>
            <label for="puppy_main_id">Dog's List</label>
            <select name='puppy_main_id' id='puppy_main_id'>
                <?php
                $puppies = array( 'post_type' => 'puppies','order' => 'asc', );
                $puppiesloop = new WP_Query( $puppies );
                while ( $puppiesloop->have_posts() ) : $puppiesloop->the_post(); ?>
                    <option value="<?php the_title(); ?>">
                        <?php the_title(); ?>
                    </option>
                <?php endwhile; ?>
            </select>
        </p>
    </form>
<?php
}

/* ADD CUSTOM META BOX For Showring Dogs */
function custom_meta_box_showring($object)
{
	
	global $post;
	$animalid = get_post_meta($post->ID,'animalId',true);
	$mydb = new wpdb('root','','dogspot','localhost');
	$rows = $mydb->get_results("select `name`,`animalid` from `name`");
	?>

    <form name="animalfrm" id="animalfrm" method="post">
      <select name="animalid" id="animalid">
      	<option value="">Select Animal</option>
        <?php foreach ($rows as $row) : ?>
        <option value="<?php echo $row->animalid;?>" <?php if($row->animalid == $animalid){ ?> selected="selected" <?php } ?>> <?php echo $row->name;?></option>
        <?php endforeach;?>
      </select>
      <input type="hidden" id="post_type" name="post_type" value="<?php echo $post_type ?>" />
      <input type="button" id="get_info" name="get_info" value="Get Info" />
    </form>
    <script type="text/javascript">
		jQuery(document).ready(function(e) {
			console.log("on load");
			jQuery('body').on('click','#get_info',function(){				
				console.log("on button click")
				var url = "<?php echo get_template_directory_uri();?>-child/~get_puppy_info.php";
				var animalid = jQuery("#animalid").val();
				var post_type = jQuery("#post_type").val();
				jQuery.ajax({
					url: url,
					type: "POST",
					//dataType:"TEXT",
					dataType:"JSON",
					data: {
						animalid:animalid,
						post_type:post_type
					},
					success: function(result) {
						jQuery("#title").val(result.name);
						jQuery("#acf-field-short_name").val(result.shortname);
						jQuery("#content").val(result.description);
						jQuery("#acf-field-sex").val(result.sex);
						jQuery("#acf-field-color").val(result.color);
						jQuery(".acf-date_picker").find('input').val(result.dob);
						jQuery("#acf-field-price").val(result.price);
						jQuery("#acf-field-photo_url").val(result.photo_url);
						var pedigree=result.pedigree;
						jQuery.each( pedigree, function( key, value ) {
							var momid=value.momid;
							var dadid=value.dadid;
							jQuery('select[name^="dam_'+key+'"] option[value="'+momid+'"]').attr("selected","selected");
							jQuery('select[name^="sir_'+key+'"] option[value="'+dadid+'"]').attr("selected","selected");
						});
					},
					error: function(e) {
					}
				});			
			});  
		});
	</script>
<?php }

/* ADD CUSTOM META BOX For Adoption */
function custom_meta_box_for_adoption($object)
{
	global $post;
	$animalid=get_post_meta($post->ID,'animalId',true);
	$mydb = new wpdb('root','','dogspot','localhost');
	$rows = $mydb->get_results("select name,animalid from name");
	?>
	<form name="animalfrm" id="animalfrm" method="post">
		<select name="animalid" id="animalid">
        	<option value="">Select Animal</option>	
		<?php foreach ($rows as $row) : ?>
			<option value="<?php echo $row->animalid; ?>" <?php if($animalid==$row->animalid){ ?> selected="selected" <?php } ?>><?php echo $row->name ?></option>
		<?php endforeach; ?>
		</select>
		<input type="button" id="get_info" name="get_info" value="Get Info" />
	</form>
	<script type="text/javascript">
        jQuery(document).ready(function(e) {
            console.log("on load");
            jQuery('body').on('click','#get_info',function(){
                
                console.log("on button click")
                var url = "<?php echo get_template_directory_uri();?>-child/~get_puppy_info.php";
                var animalid = jQuery("#animalid").val();
    
                jQuery.ajax({
                    url: url,
                    type: "POST",
                    dataType:"JSON",
					//dataType:"TEXT",
                    data: {
                        animalid:animalid,
                    },
                    success: function(result) {
                        console.log(result);
						//alert(result);						
                        jQuery("#title").val(result.name);
                        jQuery("#acf-field-short_name").val(result.shortname);
                        jQuery("#content").val(result.description);
                        jQuery("#acf-field-sex").val(result.sex);
                        jQuery("#acf-field-color").val(result.color);
                        jQuery(".acf-date_picker").find('input').val(result.dob);
                        jQuery("#acf-field-price").val(result.price);
						jQuery("#acf-field-photo_url").val(result.photo_url);
                        jQuery("#acf-vaccination_info").val(result.vaccination_type);
                    },
                    error: function(e) {
                    }
                });			
            });  
        });
    </script>
<?php } 

/* ADD CUSTOM META BOX For Pedigree Information */
function custom_meta_box_pedigree($object)
{
	
	global $wpdb;
	global $post;
	
	$post_ID = $post->ID;
	$pedigree_info = get_post_meta($post_ID, 'pedigree_info',true);
	
	$args = array(
		'posts_per_page'   => -1,
		'post_type'        => 'showring',
		'post_status'      => 'publish',
	);
	$posts_array = get_posts( $args );
	
	/*echo "<pre>";
	print_r($posts_array);
	echo "</pre>";*/

	?>
    <table class="custom_form-table" cellpadding="0" cellspacing="0" border="1" width="100%">
        <tr>
            <th width="20%">Generation</th>
            <th width="30%">Dame</th>
            <th width="30%">Sire</th>
        </tr>

		<?php
			$dam_0 = get_post_meta($post_ID, 'dam_0',true);
			$dam0 = get_post_meta($post_ID, 'dam0',true);
			$sir_0 = get_post_meta($post_ID, 'sir_0',true);
			$sir0 = get_post_meta($post_ID, 'sir0',true);
			$dam_1 = get_post_meta($post_ID, 'dam_1',true);
			$dam1 = get_post_meta($post_ID, 'dam1',true);
			$sir_1 = get_post_meta($post_ID, 'sir_1',true);
			$sir1 = get_post_meta($post_ID, 'sir1',true);
			$dam_2 = get_post_meta($post_ID, 'dam_2',true);
			$dam2 = get_post_meta($post_ID, 'dam2',true);
			$sir_2 = get_post_meta($post_ID, 'sir_2',true);
			$sir2 = get_post_meta($post_ID, 'sir2',true);
		
			for($i=0;$i<3;$i++){ 
			
				$generation=$i+1;
				$ped_key=$i;
				$dam_select = "dam_".$i;
				$dam_text = "dam".$i;
				$sir_select = "sir_".$i;
				$sir_text = "sir".$i;
				?>
				<tr> 
					<td> <?php echo $generation ?> Generation </td>
					<td>
						<select name="dam_<?php echo $ped_key ?>" id="dam_<?php echo $ped_key ?>">
							<option value="">Select</option>
							<?php 
							foreach($posts_array as $t_post) { ?>
								<option value="<?php echo $t_post->ID;?>" <?php if(isset($$dam_select)) { if($s_post->ID == $$dam_select){ ?> selected="selected" <?php } } ?>>
									<?php echo $t_post->post_title;?>
                                </option>
							<?php } ?>
						</select>
						<input type="text" id="dam<?php echo $ped_key ?>" name="dam<?php echo $ped_key ?>" value="<?php echo $$dam_text;?>"/>
					</td>
					<td>
						<select name="sir_<?php echo $ped_key ?>" id="sir_<?php echo $ped_key ?>">
							<option value="">Select</option>
							<?php foreach($posts_array as $s_post) { ?>
								<option value="<?php echo $s_post->ID;?>" <?php if(isset($$sir_select)) {  if($s_post->ID == $$sir_select){ ?> selected="selected" <?php } } ?>>
									<?php echo $s_post->post_title;?>
                                </option>
							<?php } ?>
						</select>
						<input type="text" id="sir<?php echo $ped_key ?>" name="sir<?php echo $ped_key ?>" value="<?php echo $$sir_text;?>"/>
					</td>
				</tr> 
		<?php } ?>
</table>
<?php }

/**
 * Post Update Hook For Vaccinaton Information
 *
**/
add_action( 'post_updated', 'dream_save_post_vaccination_info', 10, 3 );
function dream_save_post_vaccination_info($post_ID, $post_after, $post_before){

	$post_type = $_POST['post_type'];
	$animalId = $_POST['animalid'];
	$photo_url = $_POST['photo_url'];
	
	

	if($post_type == 'adoption' || $post_type == 'showring' || $post_type == 'puppies'){
		
		if($_POST['fields']['field_55682cd23cfe5'] != ""){
		
			// ************ Upload featured image code START here ************* //
				$imageurl = 'http://dream-a-dream.com' . $_POST['fields']['field_55682cd23cfe5'];
			
			   // $image_url  = $value; // Define the image URL here
				$upload_dir = wp_upload_dir(); // Set upload folder
				if($imageurl != '')
				{
				$image_data = file_get_contents($imageurl); // Get image data
				}
				$filename  = basename($imageurl); // Create image file name
			   
				// Check folder permission and define file location
				if( wp_mkdir_p( $upload_dir['path'] ) ) {
					$file = $upload_dir['path'] . '/' . $filename;
				} else {
					$file = $upload_dir['basedir'] . '/' . $filename;
				}
			   
			   
				// Create the image  file on the server
				file_put_contents($file, $image_data);
			   
				// Check image file type
				$wp_filetype = wp_check_filetype( $filename, null );
			   
				// Set attachment data
				$attachment = array(
					'post_mime_type' => $wp_filetype['type'],
					'post_title'     => sanitize_file_name( $filename ),
					'post_content'   => '',
					'post_status'    => 'inherit'
				);
			   
				// Create the attachment
				$attach_id = wp_insert_attachment( $attachment, $file, $post_ID );
				add_post_meta($attach_id, '_wp_attachment_image_alt', $imagealttag);
				// Include image.php
				require_once(ABSPATH . 'wp-admin/includes/image.php');
			   
				// Define attachment metadata
				$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
			   
				// Assign metadata to attachment
				wp_update_attachment_metadata( $attach_id, $attach_data );
			   
				// And finally assign featured image to post
				set_post_thumbnail( $post_ID, $attach_id );
				
				// ************ Upload featured image code END here ************* //
				
						
			update_post_meta($post_ID,'animalId',$animalId);
		
		}
	}
	
	if($post_type == 'puppies') {

		$mydb = new wpdb('root','','dogspot','localhost');
		
		$sql_vacc="SELECT vaccination_type.name, animal_vaccination.givendate, ".
		"animal_vaccination.notes FROM animal_vaccination ".
		"JOIN vaccination ON `animal_vaccination`.vacid = vaccination.vacid ".
		"JOIN vaccination_type ON vaccination.vactypeid = vaccination_type.vactypeid ".
		"WHERE animal_vaccination.animalid = '".$animalId."' ".
		"ORDER BY animal_vaccination.givendate DESC";
		
		$animal_vaccinations = $mydb->get_results($sql_vacc,ARRAY_A);

		foreach($animal_vaccinations as $animal_vacci){
		
			$post_title = $animal_vacci['notes'];
			if($post_title == ""){
				$post_title = "Empty Notes";
			} else {
				$post_title = $animal_vacci['notes'];
			}

			$my_custom_post = array(
				'post_title'=> $post_title,
				'post_status' => 'publish',
				'post_author' => 1,
				'post_type' => 'vaccinationinfo',
				'post_parent' => $post_ID				 
			);
				
			// Fetch all terms of vaccination type
			$vaccination_types = get_terms('vaccination_types', array( 'hide_empty' => false ) );
			foreach($vaccination_types as $vaccination_type){
				if($vaccination_type->name == $animal_vacci['name']){
					$vaccination_id = $vaccination_type->term_id;
				}
			}
			
			// Insert the post into the database
			$mypostid = wp_insert_post( $my_custom_post );
			
			
			
			update_post_meta($mypostid,'date_of_dos',$animal_vacci['givendate']);
			wp_set_post_terms( $mypostid, $vaccination_id, 'vaccination_types' );
				
		}
	}
	elseif($post_type == 'showring') {
		update_post_meta($post_ID,'dam_0',$_POST['dam_0']);
		update_post_meta($post_ID,'dam0',$_POST['dam0']);
		update_post_meta($post_ID,'sir_0',$_POST['sir_0']);
		update_post_meta($post_ID,'sir0',$_POST['sir0']);
		update_post_meta($post_ID,'dam_1',$_POST['dam_1']);
		update_post_meta($post_ID,'dam1',$_POST['dam1']);
		update_post_meta($post_ID,'sir_1',$_POST['sir_1']);
		update_post_meta($post_ID,'sir1',$_POST['sir1']);
		update_post_meta($post_ID,'dam_2',$_POST['dam_2']);
		update_post_meta($post_ID,'dam2',$_POST['dam2']);
		update_post_meta($post_ID,'sir_2',$_POST['sir_2']);
		update_post_meta($post_ID,'sir2',$_POST['sir2']);
	}
}
////////////////////////////ADD WIDGET FOR ARCHIVE PAGES//////////////////////////////

/**
 * Define theme's widget areas.
 *
 */
function petcare_widgets_init1() {

    register_sidebar(
        array(
            'name'          => __('Puppies Page Recent posts', 'petcare'),
            'id'            => 'puppies-post',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => "</div>",
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
	register_sidebar(
        array(
            'name'          => __('Showring Page Recent posts', 'petcare'),
            'id'            => 'showring-post',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => "</div>",
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
	register_sidebar(
        array(
            'name'          => __('Adoption Page Recent posts', 'petcare'),
            'id'            => 'adoption-post',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => "</div>",
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );

}
add_action('init', 'petcare_widgets_init1');
/////////////////////////////////////////////////////
//////////////////Add image size///////////////////////
add_action( 'after_setup_theme', 'setup' );
	function setup() {
    add_theme_support( 'post-thumbnails' ); // This feature enables post-thumbnail support for a theme
    add_image_size( 'featured-image', 555, 365, true );
}
?>