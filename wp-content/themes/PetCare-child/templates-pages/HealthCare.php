<?php
/*
Template Name: Health Care Page
*/
get_header(); 
global $wpdb;

include(TEMPLATEPATH . '/includes/inner-nav.php'); 
$post_id =  $_REQUEST['id'];

$vacc_args = array(
	'orderby'           => 'name', 
	'order'             => 'ASC',
	'hide_empty'        => false, 
	'number'            => '', 
	'fields'            => 'all', 
	'child_of'          => 0,
); 

$vacc_terms = get_terms('vaccination_types', $vacc_args);
$total_vtype=count($vacc_terms)+3;

?>
<style>
table.vacrecord,
table.weightrecord
{
	border-collapse:collapse;
	width:98%;
	margin-left:1%;
	margin-right:1%;
}
table.weightrecord
{
	width:70%;
	margin-left:15%;
	margin-right:15%;
}
table.vacrecord th,
table.vacrecord td,
table.weightrecord th,
table.weightrecord td
{
	border: 1px solid black;
}
th.vacnoteheader {
	width:300px;
}
textarea.vacnotes {
	width:300px;
}
tr.animalinfohdr,
tr.weightinfohdr {
	background-color:#99FF00;
}
td.vacdate,
td.vacage {
	
}
td.vacnotetext,
td.vaccinationDateSelector,
td.animalselectorcell {
	text-align:left;
}
td.legendLabel {
	text-align:left;
}
th.noprint h1{
	color: #000;
	font-size: 12px;
	font-weight: bold;
	height: 0;
	letter-spacing: 1px;
	margin: 103px 0 13px;
	padding: 0;
	text-transform: capitalize;
	transform: rotate(270deg);
	width: 12px;
}
th{
	text-align:center;
	color:#000;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-12 centered title-margin">
			<h3 class="border"><span><?php the_title(); ?></span></h3>
		</div>
        <div class="col-md-12">
            <table class="vacrecord">
                <tbody>
                
                <tr class="animalinfohdr" style="width:100%; background:#0F0;">
                    <th colspan="<?php echo $total_vtype; ?>"><?php echo the_field('short_name' ,$post_id); ?><br>
                        Sex:&nbsp;&nbsp;<?php echo the_field('sex',$post_id); ?><br>
                        DOB:&nbsp;&nbsp;<?php echo the_field('dob',$post_id); ?><br>
                    </th>    
                </tr>
                <tr>
                    <th>Age</th>
                    <th>Date</th>
                    <?php
                    foreach($vacc_terms as $vterm){         
                        $vterm_name=str_replace(" ","<span>&nbsp;</span>",$vterm->name);        
						?>
						<th class="noprint"><h1><?php echo $vterm_name; ?></h1></th>
					<?php } ?>
                    <th>Notes</th>
            	</tr>
				<?php
                    $DOB = get_post_meta($post_id,'dob',true);
                    
					$datesql = "SELECT pm.meta_value from ".
					"wp_postmeta as pm JOIN wp_posts as p ".
					"ON `p`.`ID`  = `pm`.`post_id` ".
					"where `p`.`post_parent` = '".$post_id."' and ".
					"`meta_key`='date_of_dos' ".
					"group by meta_value order by meta_value desc";
					
					
					$all_vacc_date = $wpdb->get_results($datesql);
					
					
					
					foreach($all_vacc_date as $dod){ 
					
						$origin_date=$dod->meta_value;
						$date_of_dos=date("Y-m-d",strtotime($dod->meta_value));
					
						?>
						
						<tr>
                            <td class="vacage"><?php echo getAge($DOB, $date_of_dos) . " Days"; ?></td>
                            <td class="vacdate"><?php echo $date_of_dos; ?></td>
                            <?php         
                            $current_notes='';
        					foreach($vacc_terms as $vterm){
								$vterm_slug=$vterm->slug;
								$vaargs = array(
									'post_type' => 'vaccinationinfo',
									'post_parent' => $post_id,
									'tax_query' => array(
										array(
											'taxonomy' => 'vaccination_types',
											'field'    => 'slug',
											'terms'    => array($vterm_slug),
										),
									),
									'meta_query' => array(
										array(
											'key'     => 'date_of_dos',
											'value'   => $origin_date,
											'compare' => 'LIKE',
										),
									),
								);
                           		$vaccquery = new WP_Query( $vaargs );
								$allposts=$vaccquery->posts;
								$vpost=$vaccquery->posts[0];
								if($vpost->post_title != 'Empty Notes' && $vpost->post_title!=''){
									 $current_notes.=$vpost->post_title."<br>";
								}
								
								
								?>
    							<td>
									<?php if(count($allposts)>0) {  
                                        echo $check_mark = '<img src="'.site_url().'/wp-content/uploads/2015/05/b_check.png"/>'; 
									} ?>
                                </td>
                           	<?php } ?>
                            <td><?php echo $current_notes; ?></td>
                    	</tr>
                	<?php } ?>
                </tbody>
            </table>
		</div>
	</div>
</div>       
<?php get_footer(); ?>