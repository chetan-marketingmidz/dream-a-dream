<?php
/*
Template Name: Pedigree page
*/
get_header();
?>
<?php include(TEMPLATEPATH . '/includes/inner-nav.php'); ?>
<?php
global $wpdb;
$mydb = new wpdb('root','','dogspot','localhost');
$post_ID=$_REQUEST['id'];
$dog_info = get_post($post_ID);


$dam_0 = get_post_meta($post_ID, 'dam_0',true);
$dam0 = get_post_meta($post_ID, 'dam0',true);
$sir_0 = get_post_meta($post_ID, 'sir_0',true);
$sir0 = get_post_meta($post_ID, 'sir0',true);
$dam_1 = get_post_meta($post_ID, 'dam_1',true);
$dam1 = get_post_meta($post_ID, 'dam1',true);
$sir_1 = get_post_meta($post_ID, 'sir_1',true);
$sir1 = get_post_meta($post_ID, 'sir1',true);
$dam_2 = get_post_meta($post_ID, 'dam_2',true);
$dam2 = get_post_meta($post_ID, 'dam2',true);
$sir_2 = get_post_meta($post_ID, 'sir_2',true);
$sir2 = get_post_meta($post_ID, 'sir2',true);
			
//echo "<pre>";
//print_r($dog_info);
//echo "</pre>";
?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/table.css" />
<div class="container">
  <div class="row">
    <div class="col-md-12 prev-next">
      <p> <span class="pre-page">< <a href="<?php echo $dog_info->guid; ?>"><?php echo get_the_title( $post_ID ); ?></a></span> </p>
    </div>
  </div>
  <div class="row adoption-single samd">
    <div class="col-md-6"> 
      <!-- Slider -->
      <div id="showring" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner"> <?php echo get_the_post_thumbnail( $post_ID, 'large' ); ?> </div>
      </div>
      <!-- Slider end --> 
    </div>
    <div class="col-md-6">
      <h2><?php echo __("Pedigree"); ?> : <?php echo get_the_title( $post_ID ); ?> </h2>
      <?php 
	  
	  //$dog1 = get_post_meta($post_ID,'puppy1 ',true);
	  

	  //echo  $dog1;
	  
	 // die();
	  
	  ?>
      <table cellpadding="0" cellspacing="0" class="pedsatyam">
        <tr>
          <th>Generation</th>
          <th>Dame</th>
          <th>Sire</th>
        </tr>
        <?php
		for($i=0;$i<3;$i++){ 
			
			$generation=$i+1;
			$ped_key=$i;
			$dam_select = "dam_".$i;
			$dam_text = "dam".$i;
			$sir_select = "sir_".$i;
			$sir_text = "sir".$i;
			?><tr>
              <td class="td1">Generation <?php echo $generation; ?></td>
              <td class="td2">
              <?php
                if( $$dam_select != ""){
                    $dam_url = get_the_permalink($post_ID);
                    ?>
                    <a href="<?php echo $dam_url; ?>"><?php echo get_the_title( $post_ID ); ?></a>
                    <?php
                } else {
                    echo $$dam_text;
                }
              ?>
              </td>
              <td class="td3">
              <?php
                if( $$sir_select != ""){
                    $sir_url = get_the_permalink($post_ID);
                    ?>
                    <a href="<?php echo $sir_url; ?>"><?php echo get_the_title( $post_ID ); ?></a>
                    <?php
                } else {
                    echo $$sir_text;
                }
              ?>
              </td>
            </tr>
            
        	<?php
		}
		?>
        
        
      </table>
    </div>
  </div>
</div>
<?php get_footer(); ?>
