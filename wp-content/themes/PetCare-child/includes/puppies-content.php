<div class="container">
	<div class="row">
		<div class="col-md-12 prev-next">
			<p>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php if( get_next_post() ) : ?>
				      <span class="next-page"><?php next_post_link('%link <span class="glyphicon glyphicon-chevron-right"></span>') ?></span>
				<?php endif; ?>
				<?php if( get_previous_post() ) : ?>
				      <span class="prev-page"><?php previous_post_link('<span class="glyphicon glyphicon-chevron-left"></span> %link') ?></span>
				<?php endif; ?>
			<?php endwhile; endif; ?>
			</p>
		</div>
	</div>
	<div class="row adoption-single">
		<div class="col-md-6">
			<!-- Slider -->
			<div id="puppies" class="carousel slide" data-ride="carousel">
					<?php if (have_posts()) : while (have_posts()) : the_post(); 
							$id =$post->ID;
					?>
                    <!-- Wrapper for slides -->
						<div class="carousel-inner">
							<?php
							$photo_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							//$photo_url = $petcare_option['photo_url'];
							$photo_alt = $petcare_option['short_name'];
								//foreach($petcare_option['opt-slides'] as $slide) {
									echo '<div class="item">';
							        echo '<img src="' . $photo_url . '" alt="' . $photo_alt . '">';
							        echo '</div>';
							    //}
						//endforeach;	?>
						</div>
						<!-- Indicators -->
						<ul class="carousel-indicators">
							<?php
								$post_counter = "-1";
								//print_r($petcare_option['opt-slides']);
								if(is_array($petcare_option['opt-slides'])){
									foreach($petcare_option['opt-slides'] as $slide) {
										$post_counter++;
										echo '<li data-target="#puppies" data-slide-to="' . $post_counter . '">';
										echo '<img src="' . $slide['image'] . '" alt="' . $slide['description'] . '">';
										echo '</li>';
									}
								}
							?>
						</ul> 
                        
                       
					<?php endwhile; endif; ?>
			</div>
			<!-- Slider end -->
		</div>
		<div class="col-md-6">
        

			<h2><?php the_title(); ?></h2>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
                <table>
                    <tr>
                        <td class="td1">Short Name</td>
                        <td class="td2">:</td>
                        <td class="td3"><?php the_field('short_name'); ?></td>
                    </tr>
                    <tr>
                        <td class="td1">Sex</td>
                        <td class="td2">:</td>
                        <td class="td3"><?php the_field('sex'); ?></td>
                    </tr>
                    <tr>
                        <td class="td1">Color</td>
                        <td class="td2">:</td>
                        <td class="td3"><?php the_field('color'); ?></td>
                    </tr>
                    <tr>
                        <td class="td1">DOB</td>
                        <td class="td2">:</td>
                        <td class="td3"><?php the_field('dob'); ?></td></tr>
                    <tr>
                        <td class="td1">Price</td>
                        <td class="td2">:</td>
                        <td class="td3"><?php the_field('price'); ?></td>
                    </tr>
                </table>
                
                
			<?php endwhile; endif; ?>
            <div class="healthcare-btn"><a href="<?php echo get_page_link(578); ?><?php echo "?id=".$id;  ?>">View Health Care</a></div>
			<div class="social-share">
				<p>Share with your friends:</p>
				<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" src="http://platform.twitter.com/widgets/tweet_button.1401325387.html#_=1402406010952&amp;count=horizontal&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fwww.klevermedia.co.uk%2Fassets%2Fpetcare%2Fadoption-single.html&amp;size=m&amp;text=PetCare%20Kennels%20-%20Responsive%20html%20template&amp;url=http%3A%2F%2Fwww.klevermedia.co.uk%2Fassets%2Fpetcare%2Fadoption-single.html" class="twitter-share-button twitter-tweet-button twitter-share-button twitter-count-horizontal" title="Twitter Tweet Button" data-twttr-rendered="true" style="width: 107px; height: 20px;"></iframe>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<a data-pin-href="http://www.pinterest.com/pin/create/button/" data-pin-log="button_pinit_bookmarklet" class="PIN_1402406011368_pin_it_button_20 PIN_1402406011368_pin_it_button_en_20_red PIN_1402406011368_pin_it_button_inline_20 PIN_1402406011368_pin_it_none_20" data-pin-config="none"><span class="PIN_1402406011368_hidden" id="PIN_1402406011368_pin_count_0"><i></i></span></a>
				<!-- Please call pinit.js only once per page -->
				
			</div>
		</div>
	</div>
</div>