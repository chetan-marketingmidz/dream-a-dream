<!-- Adoption -->
<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3 class="border"><span><?php echo $petcare_option['Adoption-title']; ?></span></h3>
			<?php echo $petcare_option['Adoption-content']; ?>
		</div>
	</div>
	<div class="row adoption">
		<?php if ($petcare_option['Adoption-listing'] == "1"){ ?>
	<!-- Display 3 Adoption -->
	<?php
	  $args = array(
	  	'post_type' => 'Adoption',
	  	'post_status' => 'publish',
	    'posts_per_page' => 3
	  );
	  $loop = new WP_Query( $args );
	  while ( $loop->have_posts() ) : $loop->the_post();
	?>
		<div class="col-md-4 wow FlipInX">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail('Adoption'); ?>
				</a>
				<div class="title">
					<h5>
						<span data-hover="<?php the_title(); ?>"><?php the_title(); ?></span>
					</h5>
				</div>
			</div>
	<?php endwhile; ?>
	
<?php } elseif ($petcare_option['Adoption-listing'] == "2"){ ?>
	
	<!-- Display 6 Adoption -->
	<?php
	  $args = array(
	  	'post_type' => 'Adoption',
	  	'post_status' => 'publish',
	    'posts_per_page' => 6
	  );
	  $loop = new WP_Query( $args );
	  while ( $loop->have_posts() ) : $loop->the_post();
	?>
		<div class="col-md-4 wow flipInX">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail('Adoption'); ?>
				</a>
				<div class="title">
					<h5>
						<span data-hover="<?php the_title(); ?>"><?php the_title(); ?></span>
					</h5>
				</div>
			</div>
	<?php endwhile; ?>
	
<?php } elseif ($petcare_option['Adoption-listing'] == "3"){ ?>
	
	<!-- Display 9 Adoption -->
	<?php
	  $args = array(
	  	'post_type' => 'Adoption',
	  	'post_status' => 'publish',
	    'posts_per_page' => 9
	  );
	  $loop = new WP_Query( $args );
	  while ( $loop->have_posts() ) : $loop->the_post();
	?>
		<div class="col-md-4">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail('Adoption'); ?>
				</a>
				<div class="title">
					<h5>
						<span data-hover="<?php the_title(); ?>"><?php the_title(); ?></span>
					</h5>
				</div>
			</div>
	<?php endwhile; ?>
	
<?php } ?>
	</div>
</div>
<!-- Adoption end -->