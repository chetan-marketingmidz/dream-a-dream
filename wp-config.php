<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dreamadream' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

//define('WP_DEBUG', true);


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '|NKDhC;K{-vYUO!{hWDxnYOK@ngX%(mYVIgRxa)iY_Vlap);aVa?[ymqWY>rX%!y^FN-Z^-j}l+KAK>W)>yhQ;BF)roTz!}hUJ-rB&&tmoSQteobgj?F<_]eKismey*m');
define('SECURE_AUTH_KEY', 'mZhnaVLocW/|t^qZqWxmelo%KHwrmlz&{a=/BJ/^@LOI]tj;+|Uq%IX/DauAz_=WOFi(e@hA[V@{ESH%?KiY^{@XNdB$QvcuUlV!Q_qC&^PyiOV![uq!%N?yJ*uMCdPF');
define('LOGGED_IN_KEY', '{&S>{AjlgU^+X%/ISfw;Z{fHBO_YY{U^[gMo>@-;L]wobE*EPXeg?aTlZU{p;IyXFBCcDB)p)QIoSrSDToLDSf>nCO*w+)_&kfIpBR!xTuU$atVn*)<-LAPPqU+p>g$!');
define('NONCE_KEY', 'ZLogQ}NZpYctiEcV&QdVShWp!yV!$@[pCnwk/bMu=XSu>@DSFUFz|]_RU=gU+dFj!s;QMZP+k}ZEt<JW[E_Gr)}}[G{CG(SpsPbr@]smL][}(-JR(L=!w[pmyWVmgACz');
define('AUTH_SALT', '@j;F;Wz^uriZIBmY*(NVjF)g!>Dyo$/zHFoZX*hKt)T?HRwOAMIDoVf*)/>kL=|MA-^dGwK;TlxHF=$XEzoCy*g%xmRaN=Q[Wh-?oOsVMGydj)d?_=Dj@Brh*A%-@p?)');
define('SECURE_AUTH_SALT', 'p+kP-b=;$aNq?@!Se<Tet-j_?<wXR|YA(p;[_Y$(@nBlQF)i!(Qe[sooIh]&unHF{b_;Ai}G^PcqFzWF-MLXsfxbdc(!Zt@Csp_<iWetDoUdChS?WLw{IBa<=HBr%/V-');
define('LOGGED_IN_SALT', 'xS[Ng(<ZhUEM$Xn!Sm?bt+MK/$k)lOouf(cnY<!);GLAc]fYX=B_aUlewWCCwe=lssbp<}t/!vwvR@uP+gx@[&^Ibsad(_l?V/-OHr!>mF)Gm=n<ZbptXIzIVQTTeDF|');
define('NONCE_SALT', 'C|s;!xlSijCfzQhE_BLNi+CazD%AfGYWUW*(|(z*T|XOI)|qR@<y+cDs>eI!e--s<$$Vh?LFLv_tmMIyL(bH+_=zP*tFdb??bkSAd<{TB}YCtS&)rSYy@Ss+KvvhZj;}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 * Include tweaks requested by hosting providers.  You can safely
 * remove either the file or comment out the lines below to get
 * to a vanilla state.
 */
if (file_exists(ABSPATH . 'hosting_provider_filters.php')) {
	include('hosting_provider_filters.php');
}
